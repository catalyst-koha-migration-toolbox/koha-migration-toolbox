#!/usr/bin/perl 

# copy_date_to_008.pl - this finds a date in 260$c and puts it into the right
# place in 008, making dates searchable and sortable in Koha.
#
# Copyright (C) 2013, 2015 Catalyst IT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use Data::Dumper;
use Modern::Perl;
use Getopt::Long;

use C4::Context;
use C4::Biblio;

my ( $start_bibnum, $end_bibnum, $verbose, $dryrun, $help );
GetOptions(
    "start=i"        => \$start_bibnum,
    "end=i"          => \$end_bibnum,
    "verbose|v"      => \$verbose,
    "dry-run|dryrun" => \$dryrun,
    "help|h"         => \$help
) or die( help() );

if ($help) {
    print help();
    exit;
}

my $context = C4::Context->new();
my $dbh     = $context->dbh();
$dbh->begin_work;

my $query = "SELECT biblionumber FROM biblioitems";
my @values;
if ( defined $start_bibnum or defined $end_bibnum ) {
    $query .= ' WHERE ';
    my @conds;
    push @conds,  "biblionumber >= ?" if defined $start_bibnum;
    push @values, $start_bibnum       if defined $start_bibnum;
    push @conds,  "biblionumber <= ?" if defined $end_bibnum;
    push @values, $end_bibnum         if defined $end_bibnum;
    $query .= join( ' AND ', @conds );
}
my $sth = $dbh->prepare($query);
$sth->execute(@values);

my ( $count, $updated ) = ( 0, 0 );
while ( my $data = $sth->fetchrow_hashref() ) {
    my $verb   = "bib $data->{biblionumber}: ";
    my $record = GetMarcBiblio( $data->{biblionumber} );
    if ($record) {
        my $date = $record->subfield( '260', "c" );
        my $field = $record->field('008');
        if ( $date =~ /([0-9]{4})/ ) {
            $date = $1;
            $verb .= "$date - ";
            my $f008_text =
                $field
              ? $field->data()
              : "130416eXXXX    xxu||||a |||| 00| 0eng d";
            my $f008 = $field // MARC::Field->new( '008', $f008_text );

            $record->insert_grouped_field($f008) unless $field;

            substr( $f008_text, 7, 4 ) = $date;
            $f008->update($f008_text);
            $verb .= $f008_text;
            ModBiblio( $record, $data->{biblionumber} );
        }
        else {
            $verb .= "no date found in '$date'";
        }
    }
    else {
        warn "Couldn't get a record for biblio $data->{biblionumber}\n";
    }
    print $verb. "\n";
}

if ($dryrun) {
    $dbh->rollback;
    warn "Changes NOT committed\n";
}
else {
    $dbh->commit;
    warn "Changes committed\n";
}

sub help {
    return <<'EOF'
This looks for a year in 260$c and copies it in to 008/07-10, what LOC calls
"Date 1", but what Koha calls pubdate. This'll allow it to sort and filter
by date.

Options:
    --start     the biblionumber to begin with
    --end       the biblionumber to end with
    --verbose   spit out more info
    --dryrun    do everything, but don't commit at the end
    --help      help

Warning: run with no arguments, this will perform this operation for every
record in the database.
EOF
}
