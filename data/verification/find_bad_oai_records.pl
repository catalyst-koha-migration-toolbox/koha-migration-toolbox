#!/usr/bin/perl 

#   find_bad_oai_records.pl - queries Koha via OAI-PMH and reports any
#   invalid records found.
#
#   Copyright (C) 2014 Catalyst IT
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#   Written by Robin Sheat <robin@catalyst.net.nz>

=head1 NAME

find_bad_oai_records.pl - fetchs all the records on a Koha system via OAI-PMH
and reports any errors that indicate a badly encoded MARC-XML document.

=head1 SYNOPSIS

B<find_bad_oai_records.pl>
B<--oai-path>=I<url>
[B<--start-biblio>=I<number>]
B<--end-biblio>=I<number>
B<--archive-id>=I<id>
[B<--output>]
[B<--threads>]
[B<--man>]
[B<--help>]

=head1 OPTIONS

=over

=item B<--oai-path>=I<url>

This is the URL to the OAI-PMH endpoint, e.g. http://koha/cgi-bin/koha/oai.pl

=item B<--start-biblio>=I<number>

The biblio number to start with, otherwise it'll start at 1.

=item B<--end-biblio>=I<number>

The biblio number to stop with.

=item B<--archive-id>=I<id>

The archive ID of the target OAI-PMH repository. In Koha, this is defined by
the OAI-PMH:archiveID system preference.

=item [B<--output>]

If supplied, a list of biblio numbers with errors will be written to here.

=item [B<--threads>]

Number of threads to run with at the same time. The default is 1, but this
will probably speed things up if your server is good.

=item [B<--help>]

Some help.

=item [B<--man>]

More help.

=back

=head1 DESCRIPTION

This will count from the start biblio number to the end biblio number, querying
Koha for each record on the way. Anything other than "the record doesn't exist"
and a MARC-XML record will generate an error.

Note that the testing for a correct record is pretty primitive, but should work
well enough.

=cut

use Modern::Perl;
use autodie;
use MCE::Loop;

use Getopt::Long;
use LWP::UserAgent;
use Pod::Usage;

my ( $oai_path, $start_biblio, $end_biblio, $output, $threads );
my ( $archive_id );
my ( $man, $help );

my $result = GetOptions(
    'oai-path=s'     => \$oai_path,
    'start-biblio=i' => \$start_biblio,
    'end-biblio=i'   => \$end_biblio,
    'archive-id=s'   => \$archive_id,
    'output=s'       => \$output,
    'threads=i'      => \$threads,
);

pod2usage( -exitstatus => 0, -verbose => 2 ) if $man;
pod2usage(1) if $help;
pod2usage(
    -message    => '--oai-path is required',
    -exitstatus => 10,
    -verbose    => 1
) if !$oai_path;
pod2usage(
    -message    => '--end-biblio is required',
    -exitstatus => 10,
    -verbose    => 1
) if !$end_biblio;
pod2usage(
    -message    => '--archive-id is required',
    -exitstatus => 10,
    -verbose    => 1
) if !$archive_id;

$start_biblio = 1 if $start_biblio < 1;
pod2usage(
    -message    => '--start_biblio must be less than --end-biblio',
    -exitstatus => 10,
    -verbose    => 1
) if $start_biblio > $end_biblio;

$threads = 1 if !$threads;

MCE::Loop::init {
    chunk_size => 100,
    max_workers => $threads,
};

my @bad_records = mce_loop {
    my ($mce, $chunk_ref, $chunk_id) = @_;

    my @results;

    my $ua = LWP::UserAgent->new;
    for my $bib (@{ $chunk_ref }) {
        my $request = HTTP::Request->new(GET => make_oai_query($bib));
        my $response = $ua->request($request);
        die "Got an HTTP error: ".$response->status_line if !$response->is_success;
        my $bad;
        eval {
            $bad = !test_biblio_ok($response->content);
        };
        if ($@) {
            die "Error: $@ testing biblio $bib\n";
        }
        push @results, $bib if $bad;
        print STDERR ($bad ? 'x' : '.');
    }
    MCE->gather(@results);
} $start_biblio .. $end_biblio;

print "\n";
open my $fh, '>', $output if $output;
foreach my $b (@bad_records) {
    print "$b\n";
    print $fh "$b\n" if $fh;
}
print @bad_records . " bad records found.\n";
close($fh) if $fh;

sub make_oai_query {
    my ($bib_num) = @_;

    return $oai_path . '?verb=GetRecord&identifier=' . $archive_id . ':' . $bib_num . '&metadataPrefix=marcxml';
}
sub test_biblio_ok {
    my ($content) = @_;

    return 0 if $content =~ /<h1>Software error:<\/h1>/;
    return 0 if $content =~ /^$/;
    return 1 if $content =~ /<datafield ind1=/;
    return 1 if $content =~ /There is no biblio record with this identifier/;

    die "Unknown result: $content\n";
}
