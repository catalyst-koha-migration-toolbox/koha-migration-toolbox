#!/bin/bash
perl -I ../../migration/Generic ../../migration/Generic/csvtomarc.pl \
    -i catalogue.csv \
    -o caanz.marc \
    -v  \
    -f usmarc \
    -m 'Systemid=marc:998_b' \
    -m 'Title=special:title' \
    -m 'func:split:|:Author=multimarc:100_a:700_a:...?' \
    -m 'Place=marc:260_a?' \
    -m 'Publisher=marc:260_b?' \
    -m 'Date=marc:260_c?' \
    -m 'Edition=marc:250_a?' \
    -m 'func:ifmatch:Book:BOOK:Australian journal:JOURNAL:Article:ARTICLE:Conference paper:CONFERENCE:Multimedia:MULTIMEDIA:Reference:REFERENCE:Item_type=marc:942_c' \
    -m 'func:ifmatch:Book:BOOK:Australian journal:JOURNAL:Article:ARTICLE:Conference paper:CONFERENCE:Multimedia:MULTIMEDIA:Reference:REFERENCE:Item_type=marc:952_y' \
    -m 'Physical_description=marc:300_a?' \
    -m 'func:split:|:Subjects=marc:650_a?' \
    -m 'Abstract=marc:520_a?' \
    -m 'Notes=marc:500_a?' \
    -m 'func:ifmatch:NOT FOR LOAN:1:Status=marc:952_7?' \
    -m 'Status=marc:952_z?' \
    -m 'oldid=marc:035_a?' \
    -m 'URL=marc:856_u?' \
    -m 'ISSN=marc:022_a?' \
    -m 'Subtitle=marc:245_b?' \
    -m 'ISBN=marc:020_a?' \
    -m 'Source=marc:490_a?' \
    -m 'Electronicfile=marc:952_x?' \
    -m 'Series=marc:440_a?' \
    -m 'MemberOffer=marc:856_u?' \
    -m 'Price=marc:952_v?' \
    -m 'HoldingLink=marc:773_9?' \
    -m 'AltCallNo=marc:084_a?' \
    -m 'FormerTitle=marc:247_a?' \
    -m 'LocalGlobal=marc:952_c?' \
    -m 'IndustryTopic=marc:654_a?' \
    -m 'func:literal:AUS=marc:952_a' \
    -m 'func:literal:AUS=marc:952_b' \
    -m 'CallNo=marc:942_h?' \
    -m 'item:Barcode=barcode?' \
    -m 'item:Volume=enumchron?' \
    -m 'item:CopyNo=copynumber?' \
    -m 'item:Call_number=itemcallnumber?' \
    -m 'item:func:literal:ddc=cn_source' \
    -m 'item:Price=replacementprice?' \
    -m 'item:Details=marc:952_x?' \
    --itemlink 'Systemid=Bibid' \
    -t 'holdings.csv'

#  -m 'Citation=marc:510_a?' \

### skipped as not useful
# Created
# Modified
# TRIMAGIC
# AddedEntries
# InternalNotes


### skipped as empty
# Frequency
# Format
# Location
# User
# Acronym
# Image
# Holdings --- only a few maybe enumchron?
# Loanperiod
# Reservedfor
# Reserveddate
# Loannotes
# Label
# CopyNo
# Icon
# Language
# Subject2
# Legislation
# Cases
# ASX_Code
# Capital_Raising
# Group
# EndDate
# MainSeriesLink
# SpecialIssuesLink
# SubSeriesLink
# PASSWORDS
# All the JVOL colums

#Note
# Date is 260c is well formated, should go in leader also



