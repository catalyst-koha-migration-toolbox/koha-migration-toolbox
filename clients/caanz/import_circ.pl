#!/usr/bin/perl 
#===============================================================================
#
#         FILE: import_circ.pl
#
#        USAGE: ./import_circ.pl
#
#  DESCRIPTION: script for importing the corrections circ data
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 04/04/13 11:46:37
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Text::CSV_XS;
use Getopt::Long;
use Date::Manip;

use C4::Context;

my $input_file;
GetOptions( 'input|i=s' => \$input_file, );

my $csv = Text::CSV_XS->new(
    {
        binary             => 1,
        eol                => $/,
        allow_loose_quotes => 1,
        escape_char        => '',
        sep_char           => ',',
        quote_char         => '"',
        auto_diag          => 2,
    }
);

my $dbh          = C4::Context->dbh();
my $borrower_sth = $dbh->prepare(
"SELECT borrowernumber, firstname, surname FROM borrowers WHERE cardnumber = ?"
);
my $item_sth = $dbh->prepare("SELECT itemnumber FROM items WHERE barcode = ?");
open my $csvfile, '<', $input_file or die "Unable to open $input_file: $!\n";

Date_Init("DateFormat=non-US");

my $circ_sth = $dbh->prepare(
"INSERT INTO issues (borrowernumber, itemnumber, date_due, issuedate, branchcode, issuingbranch) VALUES (?,?,?,?,?,?)"
);

# $dbh->prepare("TRUNCATE issues")->execute;

# $dbh->prepare("TRUNCATE oldissues")->execute;

while ( my $row = $csv->getline($csvfile) ) {
    $borrower_sth->execute( 'A' . $row->[8] );
    my $borrower = $borrower_sth->fetchrow_hashref();
    if ($borrower) {
        $item_sth->execute( $row->[10] );
        my $item      = $item_sth->fetchrow_hashref();
        if ($item){
        my $issuedate = ParseDate( $row->[12] );
        my $duedate   = ParseDate( $row->[13] );
        $duedate   = UnixDate( $duedate,   '%Y-%m-%d' );
        $issuedate = UnixDate( $issuedate, '%Y-%m-%d' );
        $circ_sth->execute( $borrower->{'borrowernumber'},
            $item->{'itemnumber'}, $duedate, $issuedate, 'AUS', 'AUS' )
          || die $circ_sth->errstr;
        }
        else {
            print "Can't find item " . $row->[10] . "\n";
        }
    }
    else {
        print "Cant find borrower " . $row->[8] . "\n";
    }

}
