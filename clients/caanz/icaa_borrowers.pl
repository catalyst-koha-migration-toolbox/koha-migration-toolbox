#!/usr/bin/perl 
#===============================================================================
#
#         FILE: icaa_borrowers.pl
#
#        USAGE: ./icaa_borrowers.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 09/01/15 09:12:35
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Getopt::Long;
use Text::CSV;
my $debug   = 0;
my $doo_eet = 0;
$| = 1;

my $input_file  = "";
my $err_file    = "";
my $output_file = "";

GetOptions(
    'in=s'  => \$input_file,
    'out=s' => \$output_file,
    'err=s' => \$err_file,
    'debug' => \$debug,
);

if ( ( $input_file eq '' ) || ( $err_file eq '' ) || $output_file eq '' ) {
    print "Something's missing.\n";
    exit;
}

my $i               = 0;
my $attempted_write = 0;
my $written         = 0;
my $other_problem   = 0;
my $broken_records  = 0;

my $csv = Text::CSV->new( { binary => 1 } );

open my $in,  "<$input_file";
open my $err, ">$err_file";
open my $out, ">", $output_file;
my $headerline = $csv->getline($in);
use Data::Dumper;
warn Dumper $headerline;

my @header = (
    'sort1',   'dateenrolled',  'sort2', 'cardnumber',
    'address', 'email',         'phone', 'firstname',
    'surname', 'borrowernotes', 'userid'
);
my $status = $csv->combine(@header);
print $out $csv->string . "\n";
while ( my $row = $csv->getline($in) ) {
    $row->[13] = ''
      if $row->[13] !~ /\@/;    #if its some number drop it out (email)
    $status = $csv->combine(
        $row->[0],  $row->[1],  $row->[2],  'A' . $row->[3],
        $row->[11], $row->[13], $row->[15], $row->[17],
        $row->[18], $row->[31], $row->[3]
    );
    print $out $csv->string . "\n";
}
