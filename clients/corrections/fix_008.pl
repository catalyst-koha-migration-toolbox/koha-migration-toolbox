#!/usr/bin/perl 
#===============================================================================
#
#         FILE: fix_008.pl
#
#        USAGE: ./fix_008.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 16/04/13 10:14:59
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use C4::Context;
use C4::Biblio;

my $context = C4::Context->new();

my $dbh = $context->dbh();

my $query = "SELECT biblionumber FROM biblioitems";
my $sth   = $dbh->prepare($query);
$sth->execute();

while ( my $data = $sth->fetchrow_hashref() ) {
    my $record = GetMarcBiblio( $data->{biblionumber} );
    my $date   = $record->subfield( '260', "c" );
    my $field  = $record->field('008');
    if ($field) {
        $record->delete_field($field);
    }
    if ( $date =~ /([0-9]{4})/ ) {
        $date = $1;

        my $f008 = MARC::Field->new( '008',
            "130416e" . $date . "    xxu||||a |||| 00| 0eng d" );
        $record->add_fields($f008);

        ModBiblio( $record, $data->{biblionumber} );
        print "Modifying biblio number: " . $data->{biblionumber} . "\n";

        #    print $f008->as_string;
    }
    else {
        print "Date is bad $date for biblio number: "
          . $data->{biblionumber} . "\n";
    }
}

