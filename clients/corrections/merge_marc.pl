#!/usr/bin/perl 
#===============================================================================
#
#         FILE: merge_marc.pl
#
#        USAGE: ./merge_marc.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 31/05/13 14:37:37
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use MARC::File::USMARC;
use MARC::File::XML;
use MARC::Record;
use MARC::Batch;
use MARC::Charset;
use C4::Context;
use C4::Biblio;

use Getopt::Long;

my ( $input_file, $output_file, $help );

my $options = GetOptions(
    'i:s' => \$input_file,
    'o:s' => \$output_file,
    'h'   => \$help,
);

sub help {
    print <<EOH;
    This de-dups and recombines objects from MARC data, using ISBN/ISSN.

    $0 -i <input file> -o <output file> [-h]
EOH
    exit;
}

help() if ($help);
help() if ( !$input_file || !$output_file );

binmode( STDOUT, ":utf8" );
my $fh = IO::File->new($input_file);

my $batch = MARC::Batch->new( 'USMARC', $fh );
$batch->warnings_off();
$batch->strict_off();
open( MARCOUT, '>', $output_file ) or die "\nFail- open marcoutput: $!";

my $select = C4::Context->dbh->prepare(
    "SELECT biblionumber,isbn FROM biblioitems WHERE ISBN = ?");
print "Processing $input_file\n";
RECORD: while () {
    my $record;
    eval { $record = $batch->next() };
    if ($@) {
        print "Bad MARC record: skipped\n";
        next RECORD;
    }
    last unless ($record);
    my $f020 = $record->field('020');
    if ($f020) {
        my @isbns = split( / /, $f020->subfield('a') );
        $select->execute( $isbns[0] );
        my $biblio = $select->fetchrow_hashref();
        if ($biblio) {
            print "Found match for "
              . $biblio->{'biblionumber'} . " "
              . $isbns[0] . "\n";
            my $koharecord = GetMarcBiblio( $biblio->{'biblionumber'} );
            my @f024       = $koharecord->field('024');
            my @f500       = $koharecord->field('500');
            my @f520       = $koharecord->field('520');
            my @f650       = $koharecord->field('650');
            my @f856       = $koharecord->field('856');
            my @f942       = $koharecord->field('942');
            my @f952       = $koharecord->field('952');
            $record->append_fields( @f024, @f500, @f520, @f650, @f856, @f942,
                @f952 );

            print MARCOUT $record->as_usmarc() if $record;
        }
        else {
            print "NOT FOUND\t" . $isbns[0] . " \n";
        }
    }
    else {
        print "No ISBN in the record";
    }
}
