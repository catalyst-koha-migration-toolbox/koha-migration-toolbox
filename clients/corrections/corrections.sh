#!/bin/bash

../../migration/Generic/csvtomarc.pl \
    -m 'ID=marc:024_a*' \
    -m 'ISBN=special:isbn?' \
    -m 'ISSN=marc:022_a?' \
    -m 'func:split:|:Author=multimarc:100_a:700_a:...' \
    -m 'func:split:|:CorpAuthor=multimarc:110_a:710_a:...?' \
    -m 'func:split:,:Title=multimarc:245_a:245_b:...' \
    -m 'Subtitle=marc:245_b?' \
    -m 'Responsibility=marc:245_c?' \
    -m 'Edition=marc:250_a?' \
    -m 'PublisherPlace=marc:260_a?' \
    -m 'func:split:|:Publisher=marc:260_b?' \
    -m 'PublicationDate=marc:260_c?' \
    -m 'func:split:;:PhysicalDescription=multimarc:300_a:300_b:300_c:...?'\
    -m 'Frequency=marc:310_a?' \
    -m 'Series=marc:490_a?' \
    -m 'Notes=marc:500_a?' \
    -m 'Table of Contents=marc:500_a?' \
    -m 'Abstract=marc:520_a?' \
    -m 'func:split:|:Keywords=marc:650_a?' \
    -m 'func:split:|:Source=multimarc:773_t:773_g?'\
    -m 'Continues=marc:780_t?' \
    -m 'Continued by=marc:785_t?' \
    -m 'URL=marc:856_u?' \
    -m 'FullText=marc:856_y?' \
    -m 'Holdings=marc:866_a?' \
    -m 'func:ifmatch:Annual report:AR:Article:AE:Book:BK:DVD:DVD:E Book:EB:E Journal:EJ:Electoric resource:ER:Journal:JL:Kit:KT:Map:MP:Newspaper:NR:Reference:REF:Sound recording:SR:Thesis:TS:Video:VO:MaterialType=marc:942_c' \
    -m 'Status=marc:952_5?' \
    -m 'func:ifmatch:Online:ON:Storage - Annual report:SAR:Storage - Dewey sequence:SDS:Storage - Journal:SJ:Storage - Series:SSER:Storage - Video:SVID:Location=marc:952_c?' \
    -m 'DateAdded=marc:952_d' \
    -m 'Classification=marc:952_o?' \
    -m 'Barcode=marc:952_p?' \
    -m 'func:ifmatch:Annual report:AR:Article:AE:Book:BK:DVD:DVD:E Book:EB:E Journal:EJ:Electoric resource:ER:Journal:JL:Kit:KT:Map:MP:Newspaper:NR:Reference:REF:Sound recording:SR:Thesis:TS:Video:VO:MaterialType=marc:952_y' \
    -m 'func:ifmatch:ARC:AR:REF:REF:Classification=ccode?'\
    -m 'func:ifmatch:DVD:AV:Sound recording:AV:Video:AV:Book:GE:E Book:GE:Electronic resource:GE:Kit:GE:Map:GE:Thesis:GE:Article:SER:E journal:SER:Journal:SER:Newspape:SER:MaterialType=ccode?'\
    -m 'CopyInfo=marc:952_z?' \
    -m 'func:literal:COR=marc:952_a' \
    -m 'func:literal:COR=marc:952_b' \
        --kohalibs=/home/chrisc/catalyst-koha \
        --kohaconf /home/chrisc/koha-dev/etc/koha-conf.xml \
        --format usmarc     \
        -o koha.marc     \
        -i corrections.csv     \
        -v

        #-m year=publicationyear? \ # source has publicationdate, no year. Could strip out.
        #-m 'classification2=skipif:^INTERLOAN'  \
        #-m country=place?   \
        #-m 'func:prefix:Article 1: :article1=append:notes?'   \
        #-m 'func:prefix:Article 2: :article2=append:notes?'   \
        #-m 'func:prefix:Article 3: :article3=append:notes?'   \
        #-m 'func:prefix:Article 4: :article4=append:notes?'   \
        #-m 'func:prefix:Article 5: :article5=append:notes?'   \
        #-m 'func:prefix:Article 6: :article6=append:notes?'   \
        #-m 'func:prefix:Journal details: :jdetails=append:notes?' \
        #-m 'func:splitcount:;:accession=special:count?' \
        #-m 'func:split:|:subject=marc:650_a?'   \
        #-m 'func:split:|:keywords=marc:651_a?'  \

# -m PublicationDate 008
# split publisher
