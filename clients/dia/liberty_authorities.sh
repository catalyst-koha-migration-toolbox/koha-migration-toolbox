perl -I ../../migration/Generic ../../migration/Generic/csvtomarc.pl \
    -m 'func:analytics=marc:001' \
    -m 'func:literal:DIA=marc:003' \
    -m 'TERM=marc:150_a' \
    --kohaconf ~/koha-dev/etc/koha-conf.xml \
    --format usmarc \
    --kohalibs /mnt/catalyst/koha \
    -o dia_subject_authorities.marc \
    -i MainSubject.CSV \
    -v
