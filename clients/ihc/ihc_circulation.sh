perl -I ../../migration/Generic ../../migration/DBTextWorks/circulation.pl \
    --infile catalogue.csv \
    --idcolumn ID \
    --idmarc 998_b \
    --namecolumn BORROWER \
    --duedatecolumn DUE \
    --branchcode IHC
