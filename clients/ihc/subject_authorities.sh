#!/bin/bash

perl -I ../../migration/Generic ../../migration/Generic/csvtomarc.pl \
        -m 'func:literal:IHC=marc:003' \
        -m subjects=marc:150_a? \
        --format usmarc     \
        -o ihc_auth.marc     \
        -i subjects.csv     \
        -v
#   -m 'func:literal:h=marc:550_w' \

