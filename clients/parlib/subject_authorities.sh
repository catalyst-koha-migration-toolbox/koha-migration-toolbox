perl -I ../../migration/Generic ../../migration/Generic/csvtomarc.pl \
    -m 'func:analytics=marc:001' \
    -m 'func:literal:PS=marc:003' \
    -m 'Subject=marc:150_a' \
    -m 'AltSubject=marc:450_a' \
    --format usmarc \
    -o parlserv_subject_authorities.marc \
    -i auth_subjects.csv \
    -v
