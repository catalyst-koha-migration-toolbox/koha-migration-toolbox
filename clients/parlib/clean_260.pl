#!/usr/bin/perl 
#===============================================================================
#
#         FILE: clean_260.pl
#
#        USAGE: ./clean_260.pl
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 03/06/15 12:07:52
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use C4::Biblio;
use C4::Context;

my $dbh = C4::Context->dbh;

my $get_sth =
  $dbh->prepare("SELECT biblionumber FROM biblio ORDER BY biblionumber");
$get_sth->execute;

while ( my $bib = $get_sth->fetchrow_hashref() ) {
    my $biblio = GetMarcBiblio( $bib->{biblionumber} );
    my $f260   = $biblio->field(260);
    if ( $f260 && !$f260->subfield('c') ) {

        # don't do anything if we dont have a 260 or if date is already set
        my $b = $f260->subfield('b');
        my ( $publisher, $date ) = split( /\,/, $b );
        $f260->update( 'b' => $publisher ) if $publisher;
        $f260->add_subfields( 'c' => $date ) if $date;
        print "Updating biblio " . $bib->{'biblionumber'} . "\n";
        ModBiblioMarc( $biblio, $bib->{biblionumber} );
    }
}

