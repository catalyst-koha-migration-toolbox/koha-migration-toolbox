perl -I ../../migration/Generic ../../migration/Generic/csvtomarc.pl \
    -m 'func:analytics=marc:001' \
    -m 'func:literal:PS=marc:003' \
    -m 'Person=marc:100_a' \
    -m 'AltPerson=marc:400_a' \
    --format usmarc \
    -o parlserv_person_authorities.marc \
    -i auth_person.csv \
    -v
