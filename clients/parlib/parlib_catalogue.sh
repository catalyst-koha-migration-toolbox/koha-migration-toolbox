#!/bin/bash
perl -I ../../migration/Generic ../../migration/Generic/csvtomarc.pl \
    -i catalogue.csv \
    -o parlib.marc \
    -v  \
    -f usmarc \
    -m 'CatISBN=marc:020_a?' \
    -m 'CatISSN=marc:022_a?' \
    -m 'func:split:|:CatCallNumber=marc:082_a?' \
    -m 'CatAuthor=marc:100_a?' \
    -m 'CatTitle=special:title?' \
    -m 'CatCorpAuthor=marc:110_a?' \
    -m 'func:split:|:CatAlternateTitle=marc:246_a?' \
    -m 'CatResponsibility=marc:245_c?' \
    -m 'CatEdition=marc:250_a?' \
    -m 'func:split:::CatPublisher=multimarc:260_a:260_b:?' \
    -m 'CatDatePublished=marc:362_a?' \
    -m 'CatPhysDesc=marc:300_a?' \
    -m 'CatSerHoldings=marc:362_a?' \
    -m 'func:split:|:CatSeriesDesc=marc:490_a?'\
    -m 'func:split:|:CatNotes=marc:500_a?' \
    -m 'func:split:|:CatAbstract=marc:520_a?' \
    -m 'func:split:|:CatLanguage=marc:546_a?' \
    -m 'func:split:|:CatSubjects=marc:650_a?' \
    -m 'func:split:|:CatOtherPerson=marc:700_a?' \
    -m 'func:split:|:CatOtherCorp=marc:710_a?' \
    -m 'func:split:|:CatSeries=marc:830_a?' \
    -m 'func:split:|:CatURL=marc:856_u?' \
    -m 'func:split:|:CatLocation=marc:852_a?' \
    -m 'func:split:|:CatStatus=marc:852_z?' \
    -m 'CatSerRetention=marc:866_a?' \
    -m 'func:literal:LENDING=marc:942_c' \
    -m 'item:ItemBarCode=barcode?' \
    -m 'item:ItemCopyInfo=stocknumber?' \
    -m 'item:ItemCallNumber=itemcallnumber?' \
    -m 'item:ItemLocation=location?' \
    -m 'item:ItemSubLocation=itemnotes?' \
    -m 'item:ItemVolume=copynumber?' \
    -m 'func:literal:PS=marc:952_a' \
    -m 'func:literal:PS=marc:952_b' \
    -m 'item:func:ifmatch:default:1:R:0:ItemNotes=marc:952_0?' \
    -m 'func:literal:LENDING=marc:952_y' \
    -m 'CatID=marc:998_b' \
    --itemlink 'CatId=ItemCatID' \
    -t 'items.csv'

### To map
# CatDateCataloged ?
# CatDateApproved  ?
# CatDateCreated
# CatDateModified
# CatWebSiteHeading
# CatWebSiteMP
# CatWebSiteMP


### skipped as not useful
# CatSerFrequency
# CatSerIssuesPerVol
# CatAuthorRef
# CatCorpAuthorRef
# CatSeriesRef
# CatSubjectsRef
# CatOtherPersonRef
# CatOtherCorpRef
# CatFlag

### skipped as empty
# CatEditor
# CatSource
# CatPlace
# CatLCCard
# CatImage
# CatFileName
# CatFullText
# CatDocsBulletin
# CatSerSpecialIssues
# CatSerIndexedIn

# NOTES
# Cant split subjects across marc, no way to know
