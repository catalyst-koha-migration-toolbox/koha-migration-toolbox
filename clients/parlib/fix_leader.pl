#!/usr/bin/perl

# script to update the leader to match the itemtype
# so far just serials

# Leader for serials is      nas a22      a 4500

use C4::Context;
use C4::Biblio;


my $context = C4::Context->new();

my $dbh = $context->dbh();

my $query = "SELECT biblionumber FROM items WHERE itype = 'SERIALS' GROUP BY biblionumber";
my $sth = $dbh->prepare($query);
$sth->execute();

while (my $data = $sth->fetchrow_hashref()){
    my $record = GetMarcBiblio($data->{biblionumber});
    $record->leader('     pas a22      a 4500');
    print "Modifying biblio number: " . $data->{biblionumber} . "\n";
    ModBiblio($record,$data->{biblionumber});
}
