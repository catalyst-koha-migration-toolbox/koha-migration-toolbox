perl -I ../../migration/Generic ../../migration/Generic/csvtomarc.pl \
    -m 'func:analytics=marc:001' \
    -m 'func:literal:PS=marc:003' \
    -m 'Corporate=marc:110_a' \
    -m 'AltCorporate=marc:410_a' \
    --format usmarc \
    -o parlserv_corporate_authorities.marc \
    -i auth_corporate.csv \
    -v
