#!/usr/bin/perl
#
# Copyright 2013 Catalyst IT Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Robin Sheat <robin@catalyst.net.nz>

# This parses the CSV-like file that comes out of Liberty and loads it as well
# as possible into Koha.

=head1 NAME

borrowers.pl - convert a CSV file containing borrowers and import it into Koha

=head1 SYNOPSIS

B<borrowers.pl>
B<--delete>
B<--input>=I<file>
[B<--fieldsep>=I<character>]
[B<--quotechar>=I<character>]
[B<--skipexisting>]
[B<--branchmap>=I<original:code> ...]
[B<--branchdefault>=I<code>]
[B<--categorymap>=I<original:code> ...]
[B<--categorydefault>=I<code>]
[B<--plugin>=I<file>]
[B<--help>] [B<--man>]

=head1 DESCRIPTION

B<borrowers.pl> takes a Liberty borrowers file and imports it into Koha. It
will overwrite all users in the database that have a borrowernumber greater
than 1. As such, it's intended for migration purposes only.

=head1 OPTIONS

=over

=item B<--delete>

This will delete all users first.

=item B<--keep-first>

When used with B<--delete>, don't delete the first borrower entry.

=item B<--input>=I<file>

The CSV file that you want to load. This can be repeated for multiple files.

=item B<--fieldsep>=I<character>

The field separator used by the CSV file.

=item B<--quotechar>=I<character>

The quote character used by the CSV file.

=item B<--skipexisting>

If set, any existing cardnumbers will be skipped, rather than causing an error.

=item B<--plugin>=I<file>

This is a perl file that can be used to do custom mapping and processing.
It should evaluate to a hash that provides a mapping for fields and attributes.

=item B<--dryrun>

Do everything, but don't commit it at the end.

=cut

use strict;
use warnings;

use C4::Context;
use Carp;
use Digest::MD5 qw/ md5_base64 /;
use Data::Dumper;
use DateTime::Format::Strptime;
use Encoding::FixLatin qw/ fix_latin /;
use File::Slurp;
use Getopt::Long;
use Koha::AuthUtils qw/ hash_password /;
use Text::CSV_XS;
use csvutils;
use autodie;

my (@input_files);

my ($ok_to_delete, $debug, $keep_first);
my ($fieldsep, $quote);
my ($help, $man);
my ($plugin_file, $skip_existing, $dry_run);

GetOptions(
    'delete'            => \$ok_to_delete,
    'keepfirst'         => \$keep_first,
    'input|i=s'         => \@input_files,
    'debug|d+'          => \$debug,
    'fieldsep=s'        => \$fieldsep,
    'quotechar=s'       => \$quote,
    'plugin|p=s'        => \$plugin_file,
    'skipexisting'      => \$skip_existing,
    'dryrun'            => \$dry_run,
    'help|h'            => \$help,
    'man'               => \$man,
);

pod2usage( -exitstatus => 0, -verbose => 2 ) if $man;
pod2usage(1) if $help;

my $fieldsep_char = csvutils::make_csv_char($fieldsep, ',');
my $quote_char = csvutils::make_csv_char($quote, '"');

die "You need to supply an input file.\n" if (!@input_files);

my %plugins;
if ($plugin_file) {
    open my $pl_fh, '<', $plugin_file;
    my $code = read_file($pl_fh);
    %plugins = eval($code);
    die "Error reading plugin: $@" if $@;
    close $pl_fh;
}

# This is a place to put bits of code when hacking the code in a proper
# way is too much hassle. # TODO should come from the plugin
my @stupid_hacks = (
);

my $dbh = C4::Context->dbh;
$dbh->{AutoCommit} = 0;
$dbh->{RaiseError} = 0;
# Truncate existing records
if ($ok_to_delete) {
    if ($keep_first) {
        $dbh->prepare('DELETE FROM issues')->execute();;
        $dbh->prepare('DELETE FROM borrowers WHERE borrowernumber<>1')->execute();
        $dbh->prepare('ALTER TABLE borrowers AUTO_INCREMENT=2')->execute();
    } else {
        $dbh->prepare('DELETE FROM issues')->execute();
        $dbh->prepare('DELETE FROM borrowers')->execute();
        $dbh->prepare('ALTER TABLE borrowers AUTO_INCREMENT=1')->execute();
    }

}

my $count=0;
my %header_to_column; # terrible global
foreach my $input_file (@input_files) {
    print "\nProcessing $input_file\n";
    my $csv = Text::CSV_XS->new({
            binary  => 1,
            eol     => $/,
            allow_loose_quotes  => 0,
            escape_char => '"',
            sep_char    => $fieldsep_char,
            quote_char  => $quote_char,
            auto_diag   => 2,
        });

    open my $csvfile, '<', $input_file
        or die "Unable to open $input_file: $!\n";

    my $header_row = $csv->getline($csvfile);
    my $hcount=0;
    %header_to_column = map { lc($_) => $hcount++ } @$header_row;

    my %field_map = %{ $plugins{fields} };

    while (my $row = $csv->getline($csvfile)) {
        if ($count % 70 == 0) {
            print "\n".$count." ";
        } else {
            print '.';
        }
        $count++;

        # Fix up anything that is stupid and custom
        foreach my $func (@stupid_hacks) {
            $func->($row);
        }

        my %fields;
        foreach my $field (keys %field_map) {
            my $dest = $field_map{$field};
            my $val = get_row_value($field, $row);
            my %f = process_dest($dest, $val);
            merge(\%fields, %f);
        }

        if ($skip_existing && (my $cardnumber = borrower_exists(%fields))) {
            warn "Skipping duplicate cardnumber $cardnumber\n";
            next;
        }
        my $id = insert_record(%fields);
    }
}

print "\n";
print(($dry_run ? "Would have committed" : "Committed") . " $count borrowers.\n");
if (!$dry_run) {
    $dbh->commit;
} else {
    $dbh->rollback;
    print "Changes not committed.\n";
}
print "\n";

# Gets a value from the row for the provided field specification. Anything
# that starts with 'literal:' is returned directly (with the 'literal:' bit
# chopped off.)
sub get_row_value {
    my ($field, $row) = @_;

    if ($field =~ /^literal:/) {
        (my $v = $field) =~ s/^literal://;
        return $v;
    } else {
        my $idx = $header_to_column{$field};
        die "Column $field is not found in the CSV.\n".Dumper(\%header_to_column) unless defined $idx;
        return $row->[$idx];
    }
}

# Given a destination field/sub/whatever and a value to go to it, this does
# whatever's necessary to process that and return the fields to go into the
# database. Extended attributes get weeded out later.
sub process_dest {
    my ($dest, $val) = @_;

    my %res;
    if (ref($dest) eq 'CODE') {
        %res = $dest->($val);
    } elsif (ref($dest) eq 'ARRAY') {
        foreach my $d (@$dest) {
            merge(\%res, process_dest($d, $val));
        }
    } else {
        %res = ($dest => $val) unless !defined($val) || $val eq '';
    }
    return %res;
}

# Given a hashref and a hash, this merges the hash into the hashref. If the
# incoming hash has values that are already in the hashref, they are
# added to an arrayref.
sub merge {
    my ($main, %new) = @_;

    foreach my $k (keys %new) {
        next unless defined $new{$k};
        if (exists($main->{$k})) {
            if (ref($main->{$k}) eq 'ARRAY') {
                push @{ $main->{$k} }, ref($new{$k}) eq 'ARRAY' ? @{ $new{$k} } : $new{$k};
            } else {
                $main->{$k} = [$main->{$k}, $new{$k}];
            }
        } else {
            $main->{$k} = $new{$k};
        }
    }
}

# This takes the fields and values array that's going to go to the database
# and checks to see if that cardnumber already exists. If it does, it returns
# the cardnumber, otherwise returns undef.
my $borrower_exists_sth;
sub borrower_exists {
    my (%fields) = @_;

    my $cardnum = $fields{cardnumber};
    $borrower_exists_sth = $dbh->prepare('SELECT cardnumber FROM borrowers WHERE cardnumber=?') unless $borrower_exists_sth;;
    $borrower_exists_sth->execute($cardnum);
    my $row = $borrower_exists_sth->fetchrow_arrayref;
    return $row ? $cardnum : undef;
}

sub insert_record {
    my (%fields) = @_;

    # Special cases for handling things
    my %specials = (
        __DEBARMENT => \&add_debarment,
    );
    # This separates the fields into ones that are attributes and ones that
    # are part of the borrower record, and routes them appropriately.
    my (%b_attribs, %borrower, @b_special);
    while (my ($key, $val) = each %fields) {
        if (exists($plugins{attributes}{$key})) {
            $b_attribs{$key} = $val;
        } elsif (exists($specials{$key})) {
            push @b_special, [$specials{$key}, $val];
        } else {
            $borrower{$key} = $val;
        }
    }
    my $id = insert_borrower(%borrower);
    insert_attribute($id, %b_attribs);
    foreach my $f (@b_special) {
        $f->[0]->($id, $f->[1]);
    }
}

sub insert_borrower {
    my (%borrower) = @_;

    my @cols = keys %borrower;
    my @values = map {
        my $v = $borrower{ $_ };
        ref($v) eq 'ARRAY' ? join( '; ', @$v ) : $v;
    } @cols;
    my $dbg_q =
        "INSERT INTO borrowers ("
      . join( ', ', @cols )
      . ") VALUES ('"
      . join( "', '", @values ) . "')";
    print "$dbg_q\n" if ($debug);
    my $q = "INSERT INTO borrowers (".join (', ', @cols).") VALUES (" .join(',', map { '?' } @cols).")";
    my $sth = $dbh->prepare($q);
    my $res = $sth->execute(@values);
    if (!defined($res)) {
        die "Database error:\n".Dumper(\%borrower);
    }
    my $id = $dbh->last_insert_id(undef,undef,undef,undef);
    return $id;
}

sub insert_attribute {
    my ($borrower_id, %attribs) = @_;

    my $q = "INSERT INTO borrower_attributes (borrowernumber, code, attribute) VALUES (?,?,?)";
    my $sth = $dbh->prepare($q);
    while (my ($key, $val) = each(%attribs)) {
        $val = [ $val ] unless ref($val);
        foreach my $att (@$val) {
            my $res = $sth->execute($borrower_id, $key, $att);
            if (!defined($res)) {
                die "Database error inserting attribute:\n".Dumper($key, $att);
            }
        }
    }
}

sub add_debarment {
    my ($borrower_id, $attribs) = @_;

    my $q = "INSERT INTO borrower_debarments (borrowernumber, comment) VALUES (?,?)";
    my $sth = $dbh->prepare($q);
    my $res = $sth->execute($borrower_id, $attribs->{comment});
    if (!defined($res)) {
        die "Failed to add debarment:\n".Dumper($borrower_id, $attribs);
    }
}

# This attempts to work out the name. It is pretty simple, and if it
# can't make sense of it, it'll just shove the results into the surname. This
# only handles the first and last names, though could be modified to handle
# initials also.
sub name {
	my ($first_db, $middle_db, $last_db, $value) = @_;

    $value =~ s/^\s*(.*?)\s*$/$1/;
	my ($first, $last) = $value =~ /^([^ ]+?) +([^ ]+?)$/;
	if (!defined($first) || !defined($last)) {
        return ([$last_db], [$value]);
    }
    return ([$first_db, $last_db], [$first, $last]);
}

# This handles names in the form of last, surname middle.
sub comma_name {
    my ($first_db, $middle_db, $last_db, $value) = @_;

    my ($last, $first, $middle) = $value =~ m/^(.*), ([^ ]*)(?: (.*))?$/;
    if (!defined($last) || !defined($first)) {
        return ([$last_db], [$value]);
    }
    return ([$first_db, $last_db], [$first, $last]) unless $middle;
    return ([$first_db, $middle_db, $last_db], [$first, $middle, $last]);
}

sub password {
    my ($db,$value) = @_;

    return ([$db], ['!']) if (!$value);
    my $digest = md5_base64($value);
    return ([$db], [$digest]);
}

sub date {
    my ($db, $default, $value) = @_;

    if (!$value || $value =~ /^0+$/ || $value eq '00/00/0000') { # blank, undef, or 0
        return () if !$default;
        return ([$db], [$default]);
    }

    my $date_out = fix_date($value);
    return ([$db], [$date_out]);
}

# This attempts to guess what date format the input is in, and convert it if
# needed. It'll detect dd/mm/yyyy and yyyy-mm-dd, and outputs the latter
# (which mysql is happy with.) If it detects a null date, it returns undef.
sub fix_date {
    my ($date) = @_;

    return undef if ($date eq '00000000' || $date eq '0000-00-00');

    if ($date =~ m|(\d\d)/(\d\d)/(\d{4})|) {
        return "$3-$2-$1";
    }

    if ($date =~ /\d{4}-\d\d-\d\d/) {
        return $date;
    }

    if ($date =~ /(\d{4})(\d{2})(\d{2})/) {
        return "$1-$2-$3";
    }

    croak "Unexpected date input: $date";
}

# This is a utility function that will build an address, given a set of rows.
# Usage:
#   my %fields = build_address($type, @rows);
# $type is one of 'primary', 'secondary', 'alternate' which puts the results
# into the normal, the 'B_' set, or the alternate contact respectively.
#
# It's biased towards NZ address forms, you may need to enhance it for your
# own local use.
sub build_address {
    my ($type, @rows) = @_;

    my %fields = (
        primary => {
            a => 'address',
            b => 'address2',
            c => 'city',
            z => 'zipcode',
        },
        secondary => {
            a => 'B_address',
            b => 'B_address2',
            c => 'B_city',
            z => 'B_zipcode',
        },
        alternate => {
            a => 'altcontactaddress1',
            b => 'altcontactaddress2',
            c => 'altcontactaddress3',
            z => 'altcontactzipcode',
        }
    );
    my %res;
    return %res unless @rows;
    # The first row always goes into the first field.
    $res{$fields{$type}{a}} = shift @rows;
    return %res unless @rows;
    # The last row may contain a post code, lets check
    my $last = pop @rows;
    my ($city, $post) = $last =~ m/^(.*) (\d+)$/;
    if (defined($city) && defined($post)) {
        $res{$fields{$type}{c}} = $city;
        $res{$fields{$type}{z}} = $post;
    } else {
        $res{$fields{$type}{c}} = $last;
    }
    return %res unless @rows;
    # Anything left gets joined up and put into the remaining field
    $res{$fields{$type}{b}} = join(', ', @rows);
    return %res;
}

# This does some mildly NZ-specific manipulation to normalise phone numbers.
# The first arg is the default area code, which is prepended to the number if
# it doesn't start with '0' or '+'. Also, all non-digits (or '+') will go away.
sub phone {
    my ($area, $db_phone, $value) = @_;

    return if !$value;

    $value =~ s/[^+\d]//g;

    return ([$db_phone], [$value]) if $value =~ /^[0+]/;

    return ([$db_phone], [$area . $value]);
}

sub clean_string {
    my $value = shift;
    # \a (^G, \x007) is a special character that gets turned into a ',' on
    # write. This lets troublesome commas be escaped.
    $value =~ s/\a/,/g;
    $value =~ s/[\r\n]/\n/g;
    # Sometime some things use funny characters for things. This is what one
    # particular liberty dump uses, if we see different things, it's time to
    # pull them out to a command line option
    $value =~ s/\xAE/\n/g;
    $value =~ s/\x96/-/g;
    # Remove some smart quote madness that gets into CSVs
    $value = fix_latin($value) if ref($value) eq '';
    if (ref($value) eq 'ARRAY') {
        $_ = fix_latin($_) foreach (@$value);
    }
    return $value;
}

# Returns true if a list is empty. That is, it has no elements, or all the
# elements it does have are undef or empty strings.
sub empty_list {
    my (@list) = @_;

    return 1 if !@list;
    foreach my $entry (@list) {
        return 0 if defined $entry && $entry ne '';
    }
    return 1;
}

# Designed to be use by a plugin, this runs through a series of strptime
# date formats until one of them matches the provided input.
sub extract_date {
    my ($output_field, @formats) = @_;

    my @dts;
    foreach my $f (@formats) {
        push @dts,
          DateTime::Format::Strptime->new(
            pattern   => $f,
            locale    => 'en_NZ',
            time_zone => 'Pacific/Auckland',
          );
    }
    return sub {
        my $val = shift;

        foreach my $f (@dts) {
            my $date = $f->parse_datetime($val);
            return ($output_field => $date->ymd) if $date;
        }
        die "Unable to parse the date: $val";
    };
}
