# This is a module to centralise a few functions that I use.

package csvutils;

use strict;
use warnings;


# This takes strings like '\t' and '0x123' and turns them into the actual
# symbol they represent. A default may be specified in case the passed in
# value is undef or blank.
sub make_csv_char {
    my ($str, $def) = @_;

    return $def if !$str;
    return "\t" if ($str eq '\t');
    return chr(hex($str)) if $str =~ /^0x/;
    return $str;
}

1;
