#!/usr/bin/perl 

# Converts a Symphony CSV file to an SQL file, changing the fields to match
# column names, and also including the extended fields that come from a
# different file.
#
# Copyright (C) 2012 Catalyst IT Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 NAME

csv_to_sql.pl - convert a Symphony CSV file to an SQL file

=head1 SYNOPSIS

B<csv_to_sql.pl>
B<--map> I<infield>:I<outfield>[~I<option>:...]
B<--extended> I<infield>:I<outfield>:I<file>:I<entrynumber>[~I<option>:...]
B<--literal> I<value>:I<outfield>
B<--input> I<infile.csv>
B<--output> I<outfile.sql>
B<--table> I<tablename>
[B<--truncate>]
[B<--man>]
[B<--help>]

=head1 DESCRIPTION

B<csv_to_sql.pl> will load a CSV and output an SQL file. This SQL is created
by reading all the I<infield>s listed by B<--map> and adding them to I<table>
with the field name given by I<outfield>.

=head1 OPTIONS

=over 4

=item B<--map> I<infield>:I<outfield>[~I<option>:...]

This is repeated for every column wanted in the output, it converts the
I<infield> CSV column to the I<outfield> database column. 

Available options:

=over 4

=item B<newline>

Stick a newline on to the end of the field.

=back

=item B<--extended> I<infield>:I<outfield>:I<file>:I<entrynumber>[~I<option>:...]

This merges in the extended file I<file> by matching the number in I<infield>
and the key I<entrynumber>. The value from there is output into outfield.

Options match those of B<--map>.

=item B<--literal> I<value>:I<outfield>

Passes I<value> straight out into I<outfield>.

=item B<--input> I<infile.csv>

Load the data from this CSV file.

=item B<--output> I<outfile.sql>

Write the SQL into this file

=item B<--table> I<tablename>

Output the SQL to apply to this table.

=item B<--truncate>

Put a C<TRUNCATE> command at the top of the SQL to clear out the table first.

=item B<--man>

Show the full documentation.

=item B<--help>

Show a usage synopsis.

=back

=head1 AUTHOR

Robin Sheat <robin@catalyst.net.nz>

=cut

use Data::Dumper;

use autodie;
use Getopt::Long;
use Modern::Perl;
use Pod::Usage;
use Text::CSV_XS;

my ( @maps, $table, @extended, @literals );
my %opts = (
    'map'      => \@maps,
    'table'    => \$table,
    'extended' => \@extended,
    'literals' => \@literals
);
my $opt_res = GetOptions(
    \%opts,       'input=s', 'output=s', 'table=s',
    'truncate',   'map=s',   'table=s',  'extended=s',
    'literals=s', 'man',     'help|h'
);

pod2usage( { -exitval => 1, -verbose => 0, -output => \*STDERR } )
  if ( !$opt_res );
pod2usage( { -exitval => 0, -verbose => 2 } ) if ( $opts{man} );
pod2usage( { -exitval => 0, -verbose => 1 } ) if ( $opts{help} );

my $err =
    !@maps         ? "At least one map needs to be supplied (--map)"
  : !$opts{input}  ? "Input file missing (--input)"
  : !$opts{output} ? "Output SQL file missing (--output)"
  : !$opts{table}  ? "Table name missing (--table)"
  :                  undef;

pod2usage(
    { -exitval => 1, -verbose => 0, -output => \*STDERR, -message => $err } )
  if ($err);

open my $in_fh,  '<:encoding(utf8)', $opts{input};
open my $out_fh, '>:encoding(utf8)', $opts{output};

say $out_fh "TRUNCATE $table;" if $opts{truncate};

my $csv = Text::CSV_XS->new();
my %h2c = header_index( $csv->getline($in_fh) );

# Turn the maps into a more useful form, also make sure they're valid here.
foreach (@maps) {
    my ( $from, $to, $options ) = $_ =~ m/^(.*):([^~]+)(?:~(.*))?$/g;
    my @options = split /:/, $options if defined $options;
    my %options = map { $_ => 1 } @options;
    die "Invalid map: $_\n" if !defined($from) || !defined($to);
    die "Source column $from not found in CSV.\n" if !exists $h2c{$from};
    $_ = [ $from, $to, \%options ];
}
foreach (@literals) {
    my ( $from, $to ) = $_ =~ m/(.*):(.*)/;
    die "Invalid literal: $_\n" if !defined($from) || !defined($to);
    $_ = [ $from, $to ];
}

my ( $ext_files, $ext_maps ) = load_extended( \@extended );

my $count = 0;
while ( my $row = $csv->getline($in_fh) ) {
    if ($count % 70 == 0) {
        print "\n$count\t";
    } else {
        print ".";
    }
    $count++;
    my %output;
    foreach my $map (@maps) {
        my $val = $row->[ $h2c{ $map->[0] } ];
        chomp $val;
        $val .= "\n" if ($map->[2]{newline});
        $output{ $map->[1] } = $val;
    }
    foreach my $ext (@$ext_maps) {
        my $offset   = $row->[ $h2c{ $ext->{infield} } ];
        my $file     = $ext->{file};
        my $entrynum = $ext->{entrynum};
        my $entry    = $ext_files->{$file}{$entrynum}{$offset};
        my $options  = $ext->{options};
        next if !$entry;
        chomp $entry;
        $entry .= "\n" if $options->{newline};
        if ( exists $output{ $ext->{outfield} } ) {
            $output{ $ext->{outfield} } .= $entry;
        }
        else {
            $output{ $ext->{outfield} } = $entry;
        }
    }
    foreach my $lit (@literals) {
        my ( $val, $outfield ) = @$lit;
        $output{$outfield} = $val;
    }
    save_sql( $out_fh, $table, \%output );
}
print "\n";
close $in_fh;

sub save_sql {
    my ( $fh, $table, $record ) = @_;

    my $fields = join( ",", keys %$record );
    my $values = "'" . join( "','", sql_escape( values %$record ) ) . "'";
    say $fh "INSERT INTO $table ($fields) VALUES ($values);";
}

sub sql_escape {
    my (@strs) = @_;

    foreach (@strs) {
        s/'/\\'/g;
        s/"/\\"/g;
    }
    return @strs;
}

# Generates a hash that maps column names to their position in the array.
sub header_index {
    my ($row) = @_;

    my $count = 0;
    map { $_ => $count++ } @$row;
}

# This loads the extended files, pulling the required information into a hash
# for easy lookup. It also returns an AoH of the supplied arguments to allow
# easy looping through.
sub load_extended {
    my ($maps) = @_;

    my %files;
    my @mapping;
    foreach my $map (@$maps) {
        my ($left, $right) = $map =~ m/^(.*?)(?:~(.*))?$/;
        my ( $infield, $outfield, $file, $entrynum ) = split /:/, $left;
        if ( !( $infield && $outfield && $file && $entrynum ) ) {
            die "Invalid extended map supplied: $map\n";
        }
        my @options = split /:/, $right if defined $right;
        my %options = map { $_ => 1 } @options;
        my %m = (
            infield  => $infield,
            outfield => $outfield,
            file     => $file,
            entrynum => $entrynum,
            options  => \%options,
        );
        push @mapping, \%m;
        $files{$file}{$entrynum} = {};
    }

    # Now we go and load the parts of the files that we'll need.
    foreach my $file ( keys %files ) {
        open my $fh, '<:encoding(utf8)', $file;

# The header of these files is:
#  "OFFSET","ENTRY_POSITION","ABSOLUTE_ENTRY","ENTRY_NUMBER","VED_PREFIX","ENTRY"
#     0            1                  2             3              4         5
#
# OFFSET contains the field that is referenced in the source file
# ENTRY_POSITION and ABSOLUTE_ENTRY will be ignored for this, I'm not
#   sure what they do.
# ENTRY_NUMBER describes the nature of the field. If we see multiple
#   fields with the same ENTRY_NUMBER we should concatenate them or
#   something.
# VED_PREFIX is "Y" or "N". Dunno why.
# ENTRY is our actual value.
        my $csv = Text::CSV_XS->new();
        $csv->getline($fh);    # discard header
        while ( my $row = $csv->getline($fh) ) {
            my $offset   = $row->[0];
            my $entrynum = $row->[3];
            my $entry    = $row->[5];
            next if !exists $files{$file}{$entrynum};
            if ( exists $files{$file}{$entrynum}{$offset} ) {
                $files{$file}{$entrynum}{$offset} .= "\n" . $entry;
            }
            else {
                $files{$file}{$entrynum}{$offset} = $entry;
            }
        }
        close $fh;
    }
    return ( \%files, \@mapping );
}
