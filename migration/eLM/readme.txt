There are two directories here, reports and export.

If all you can do to get your data out is to use reports, then use the code
in there. Note that a lot of data doesn't get export, and when it does it's
not done well enough to do reliable matching of biblios to items, and such.

If (and this is the recommended way) you can get Contec to make you an export
of all your data, then use the stuff in export. It's a much more complete
format.
