#!/usr/bin/perl 

# This script will take the ouput of a .eLM system (in XML, generated using
# the reporting system) and convert it into MARC. For the most part, it just
# takes the MARC-like XML and turns it into real MARC.
#
# Robin Sheat <robin@catalyst.net.nz>

=head1 NAME

elm.pl - converts the XML biblio report export from .eLM and converts it to MARC

=head1 SYNOPSIS

B<elm.pl>
B<--biblio>=I<file>
B<--items>=I<file>
B<--misseditems>=I<file>
B<--output=I<file>

=head2 Usage

=cut

use strict;
use warnings;

use Data::Dumper;
use Getopt::Long;
use MARC::Record;
use MARC::File::XML ( BinaryEncoding => 'utf8' );
use Pod::Usage;

use XML::SAX;
use XML::LibXML::SAX;

use Elm::BiblioHandler;
use Elm::BorrowerHandler;
use Elm::ItemHandler;
use Elm::ItemMatcher;

my ( $biblio_file, $items_file, $misseditems_file, $output_file, $help );
my ( $borrower_file, $borrower_output_file );

GetOptions(
    'biblio=s'       => \$biblio_file,
    'items=s'        => \$items_file,
    'misseditems=s'  => \$misseditems_file,
    'output=s'       => \$output_file,
    'borrowers=s'    => \$borrower_file,
    'borrowersout=s' => \$borrower_output_file,
    'help|h'         => \$help,
) or pod2usage(2);

pod2usage(1) if $help;

pod2usage("A biblio file is required.\n")      if !$biblio_file;
pod2usage("An output file is required.\n")     if !$output_file;
pod2usage("An items file is required.\n")      if !$items_file;
pod2usage("A misseditems file is required.\n") if !$misseditems_file;
pod2usage("borrowers and borrowersout must both be provided if one is.\n")
  if ( !$borrower_file != !$borrower_output_file );

if ($borrower_file) {
    open( my $borr_out_fh, '>', $borrower_output_file )
      or die "Unable to open $borrower_output_file: $!\n";
    my @fields =
      qw(cardnumber surname firstname streetnumber address city email phone dateofbirth branchcode categorycode sex debarred password);
    print $borr_out_fh join( ',', @fields ) . "\n";
    my $borr_handler = Elm::BorrowerHandler->new;
    $borr_handler->set_borrower_callback(
        sub {
            my ($borr_data) = @_;
            my @data = map {
                my $d = $borr_data->{$_} || '';
                $d =~ s/"//g; # This will have to change
                $d =~ s/[\r\n]//g;
                $d;
            } @fields;
            print $borr_out_fh '"' . join( '","', @data ) . "\"\n";
            print '.';
        }
    );
    my $borrower_parser = XML::SAX::ParserFactory->parser(Handler=>$borr_handler);
    eval {
    	$borrower_parser->parse_uri($borrower_file);
    };
    if ($@) {
    	print "\n";
        warn "Loading borrowers did NOT complete: $@\n";
        exit;
    }
    print "\n\n";
close $borr_out_fh;
}
print "Loading items\n";

my %stats = ( count => 0, no_items => 0, items => 0);

my $output = open_output_file($output_file);
open( my $misseditem_fh, '>', $misseditems_file )
  or die "Unable to open $misseditems_file: $!\n";
print $misseditem_fh "barcode,author,reason";

my $item_handler = Elm::ItemHandler->new;
$item_handler->set_record_callback( \&new_item );

my $biblio_handler = Elm::BiblioHandler->new;
$biblio_handler->set_record_callback( \&new_record );
my $item_matcher = Elm::ItemMatcher->new( missed_item => \&missed_item );
my $item_parser = XML::SAX::ParserFactory->parser( Handler => $item_handler, );
eval {
    $item_parser->parse_uri($items_file);
    print "\n";
};
if ($@) {
    print "\n";
    warn "Loading items did NOT complete: $@\n";
    exit;
}
print "\nStarting biblios.\n";
my $biblio_parser =
  XML::SAX::ParserFactory->parser( Handler => $biblio_handler, );

eval { $biblio_parser->parse_uri($biblio_file); };
if ($@) {
    print "\n";
    warn "Conversion did NOT complete: $@\n";
    exit;
}

$output->close;
print $misseditem_fh
"\n\n=============================================\nauthor,title,year,classification,reason\n";
foreach my $missed ( $item_matcher->unused_items ) {
    no warnings;
    print $misseditem_fh
"$missed->[0] ~ $missed->[1] ~ $missed->[2] ~ $missed->[3] ~ Unable to find a matching biblio record\n";
}
print $misseditem_fh
  "\n\n=============================================\nbarcode,reason\n";
foreach my $missed ( $item_matcher->duplicate_barcodes ) {
    print $misseditem_fh "$missed,Duplicate barcode\n";
}
close $misseditem_fh;
print "\n$stats{count} biblios written.\n";
print "$stats{items} items written.\n";
print "$stats{no_items} biblios with no items.\n";
print $item_matcher->unused_items . " items remain unallocated.\n";

sub new_record {
    my ($record) = @_;

    if ( $stats{count} % 70 == 0 ) {
        print "\n$stats{count}  ";
    }
    else {
        print ".";
    }
    $stats{count}++;
    $item_matcher->attach_items($record);
    my @it = $record->field('952');
    $stats{items} += scalar(@it);
    $stats{no_items}++ if !scalar(@it);
    write_record( $output, $record );
}

sub new_item {
    my ($item) = @_;

    $item_matcher->new_item($item);
}

sub open_output_file {
    my ($filename) = @_;

    MARC::File::XML->default_record_format('MARC21');
    return MARC::File::XML->out($output_file);
}

sub write_record {
    my ( $handle, $record ) = @_;
    eval { $handle->write($record); };
    if ($@) {
        die "Failed to write record: $@\n" . Dumper($record);
    }
}

sub missed_item {
    my ( $item, $reason ) = @_;

    print $misseditem_fh "$item->{barcode},$item->{author},$reason\n";
}
