#!/usr/bin/perl 

# This can be used to clean up an .eLM input file before running it through
# an XML parser.
#
# It hopefully converts and cleans up all the crazy stuff that ends up in
# these things.
# 
# Options:  --input: the input file
#           --output: the output file

use strict;
use warnings;
use utf8;

use Encoding::FixLatin qw(fix_latin);;
use Getopt::Long;

my ($infile, $outfile);

GetOptions(
    'input=s'   => \$infile,
    'output=s'  => \$outfile,
);

die "Both --input and --output need to be provided.\n" if !$infile || !$outfile;

open(my $infh, '<:encoding(latin1)', $infile) || die "Unable to open $infile for reading: $!\n";
open(my $outfh, '>:utf8', $outfile) || die "Unable to open $outfile for writing: $!\n";
my $last_was_detail = 0;
my %seen = ();
while (<$infh>) {
    s/\xb9//g;
    s/\x81//g;
    s/\x92/'/g;
    s/\x1b//g;
    $_ = fix_latin($_);
    s/\xC3/Ã/g;
    s/\xe6/æ/g;
    s/\x00e/é/g;
    s/\x00o/ó/g;
    s/\x00a/á/g;
    s/\x00u/ú/g;
    s/e\xb4/é/g;
    s/a\xb4/á/g;
    s/o\xb4/ó/g;
    s/u\xb4/ú/g;
    s/i\xb4/í/g;
    s/A\xb4/Á/g;
    s/n\xb4/ń/g;
    s/e\xa8/ë/g;
    s/a\xa8/ä/g;
    s/i\xa8/ï/g;
    s/o\xa8/ö/g;
    s/u\xa8/ü/g;
    s/c\xb8/ç/g;
    s/a\xAF/ā/g;
    s/e\xAF/ē/g;
    s/i\xAF/ī/g;
    s/o\xAF/ō/g;
    s/u\xAF/ū/g;
    s/\x00//g;
    if ($last_was_detail) {
    	$last_was_detail = 0;
        # We throw the line into a hash so what we can skip it if we've seen
        # it before.
        next if $seen{$_};
        $seen{$_} = 1;
    }
    $last_was_detail = (/^<detail>$/);
    print $outfh $_;
}
close($infh);
close($outfh);
