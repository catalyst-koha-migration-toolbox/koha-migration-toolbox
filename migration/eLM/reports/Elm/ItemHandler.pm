package Elm::ItemHandler;

# This handles .eLM item XML files.

use strict;
use warnings;

use base qw(XML::SAX::Base);

use Data::Dumper;

use MARC::Record;

sub start_document {
    my ( $self, $doc ) = @_;
}

# Set to 1 to pull in as a field, 0 to ignore.
my %extrafields = (
    author                => 0,
    biblioType            => 1,
    biblioTypeCode        => 1,
    classification        => 0,
    classificationBiblio  => 0,
    classificationGeneral => 1,
    classificationItem    => 1,
    isActiveAndCurrent    => 0,
    isActiveAndDeleted    => 0,
    isAvailable           => 0,
    isLost                => 1,
    isOnIssue             => 0,
    isSerial              => 0,
    isStack               => 1,
    isWaiting             => 0,
    isDeleted             => 1,
    _082bDeweyItemNumber  => 0,
);

sub start_element {
    my ( $self, $el ) = @_;
    my $name = $el->{Name};

    # This lets us ignore things
    return if exists( $extrafields{$name} ) && !$extrafields{$name};

    # Things starting with _ are the fields for the item
    if ( $name =~ /^_/ || $extrafields{$name} ) {
        $self->start_field($el);
        return;
    }

    # 'detail' starts a block of fields
    if ( $name eq 'detail' ) {
        $self->start_record($el);
        return;
    }

    # 'items' is the root element
    return if $name eq 'items';

    die "Unknown items element name found: $name\n";
}

sub end_element {
    my ( $self, $el ) = @_;

    my $name = $el->{Name};

    # This lets us ignore things
    return if exists $extrafields{$name} && !$extrafields{$name};

    if ( $name =~ /^_/ || $extrafields{$name} ) {
        $self->end_field($el);
        return;
    }

    if ( $name eq 'detail' ) {
        $self->end_record($el);
        return;
    }

    # 'items' is the root element
    return if $name eq 'items';

    die "Unknown ending items element name found: $name\n";
}

sub end_document {
    my ( $self, $doc ) = @_;

}

sub characters {
    my ( $self, $chars ) = @_;

    return unless $self->{inside_field};
    $self->{characters} .= $chars->{Data};
}

# === End of XML::SAX::Base hooks ===

sub start_record {
    my ( $self, $el ) = @_;

    $self->{record}       = {};
    $self->{inside_field} = 0;
    $self->{characters}   = '';
    $self->{skip_record}  = 0;
}

sub start_field {
    my ( $self, $el ) = @_;

    $self->{inside_field} = 1;
    $self->{characters}   = '';
}

my %field_to_name = (
    '_100aItemId'              => 'barcode',
    '_200aLastSeenDate'        => 'lastseen',
    '_200bDueDateAndTime'      => 'duedate',
    '_200cCurrentBorrower'     => 'currentborrower',
    '_500aItemType'            => 'itemtype',
    '_500cCurrentBranch'       => 'currentbranch',
    '_500dAccessionDate'       => 'accessiondate',
    '_500hPurchasePrice'       => 'purchaseprice',
    '_500iPrice2'              => 'anotherprice',
    '_500jPrice3'              => 'yetanotherprice',
    '_500mSupplier'            => 'supplier',
    '_500nVolume'              => 'volume',
    '_500oClassification'      => 'classification',
    '_500sNotesTrap'           => 'notes',
    '_500tPurchaseDate'        => 'purchasedate',
    '_501iStatus'              => 'status',
    '_502aStocktakeDate'       => 'stocktakedate',
    '_502bStocktakeStatu'      => 'stocktakestatus',
    '_245aTitle'               => 'title',
    '_245bSubtitle'            => 'subtitle',
    '_100aPersonalName'        => 'author',
    '_020aISBN'                => 'isbn',
    '_082aDeweyClassification' => 'classification',
    'biblioType'               => 'recorddetails',
    'biblioTypeCode'           => 'bibliotypecode',
    'classificationGeneral'    => 'classificationgeneral',
    'classificationItem'       => 'classificationitem',
    'isLost'                   => sub { ( 'itemlost', shift eq 'true' ) },
    'isStack'                  => sub { ( 'location', shift eq 'true' ) },
);

sub end_field {
    my ( $self, $el ) = @_;

    $self->{inside_field} = 0;
    return if $self->{skip_record};

    # map the field tag name to something more meaningful and add it to the
    # record we're building up.
    my $field = $el->{Name};
    my $chars = $self->{characters};

    if ( $field eq 'isDeleted' ) {
        $self->{skip_record} = $chars eq 'true';
        return;
    }

    my $name = $field_to_name{$field}
      || die "Unknown item field found: $field\n";

    # If the "name" is a reference, then it returns a sub to give us our field.
    if ( !ref $name ) {
        $self->{record}{$name} = $self->{characters};
    }
    else {
        my ( $f, $value ) = $name->($chars);
        $self->{record}{$f} = $value if $value;
    }
}

sub end_record {
    my ( $self, $el ) = @_;
    return if $self->{skip_record};
    $self->{record_callback}->( $self->{record} );
}

sub set_record_callback {
    my ( $self, $callback ) = @_;

    $self->{record_callback} = $callback;
}
