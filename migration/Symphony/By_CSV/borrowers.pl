#!/usr/bin/perl 

# Converts Unicorn borrower information into SQL statements for Koha

# Copyright (C) 2012 Catalyst IT Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Unicorn has a number of tables containing user data. This reads them, links
# them together, and outputs SQL to be imported into Koha.

=head1 NAME

borrowers.pl - Convert Unicorn borrowers to Koha SQL

=head1 SYNOPSIS

B<borrowers.pl>
B<--users> I<users_file.csv>
B<--userxinfo> I<userxinfo_file.csv>
B<--output> I<koha_borrowers.sql>
[B<--savemin> I<borrower number>]
[B<--savemax> I<borrower number>]
[B<--nodelete>]
[B<--man>]
[B<--help>]

=head1 DESCRIPTION

B<borrowers.pl> takes the users data file, mixes in the users extended
information file, and outputs SQL to be imported into Koha. By default it
produces commands to delete the existing borrowers, however B<--savemin>,
B<--savemax>, and B<--nodelete> allow ranges to be kept.

=head1 OPTIONS

=over 4

=item B<--users> I<users_file.csv>

Specifies the file containing the core user data.

=item B<--userxinfo> I<usersxinfo_file.csv>

Specifies the file containing the extended user data.

=item B<--output> I<koha_borrowers.sql>

This is the name of the file containing the SQL that can be imported into
Koha.

=item B<--savemin> I<borrower number>

Borrower numbers below this will be deleted.

=item B<--savemax> I<borrower number>

Borrower numbers above this will be deleted. It is expected that this
will be combined with B<--savemin> to cause some key accounts to be not
deleted.

=item B<--nodelete>

Don't delete any existing users. This will override B<--savemin> and
B<--savemax>, causing them to be ignored.

=item B<--man>

The full documentation.

=item B<--help>

A usage synopsis.

=back

=head1 AUTHOR

Robin Sheat <robin@catalyst.net.nz>

=cut

use autodie;
use Data::Dumper;
use Digest::MD5 qw(md5_base64);
use Getopt::Long;
use Modern::Perl;
use Pod::Usage;
use Text::CSV_XS;

my %opts;

my $opt_res = GetOptions(
    \%opts,          'users|u=s',     'userxinfo|x=s', 'output|o=s',
    'savemin|min=i', 'savemax|max=i', 'nodelete|n',    'man',
    'help|h'
);

pod2usage( { -exitval => 1, -verbose => 0, -output => \*STDERR } )
  if ( !$opt_res );
pod2usage( { -exitval => 0, -verbose => 2 } ) if ( $opts{man} );
pod2usage( { -exitval => 0, -verbose => 1 } ) if ( $opts{help} );

my $err =
    !$opts{users}     ? "Users data file missing (--users)"
  : !$opts{userxinfo} ? "Users extended data file missing (--userxinfo)"
  : !$opts{output}    ? "Output SQL file missing (--output)"
  :                     undef;

pod2usage(
    { -exitval => 1, -verbose => 0, -output => \*STDERR, -message => $err } )
  if ($err);

open my $out_fh, '>:encoding(utf8)', $opts{output};
write_delete($out_fh, $opts{savemin}, $opts{savemax}) if !$opts{nodelete};

# Data processing starts from here.
my $ext_data = load_ext_data($opts{userxinfo});

my $csv = Text::CSV_XS->new();
open my $fh, "<:encoding(utf8)", $opts{users};
# Grab the header and put it into a hash so we can use field names.
my $header = $csv->getline($fh);
my $count=0;
my %h2c = map { $_, $count++ } @$header;
while (my $row = $csv->getline($fh)) {
    my %borr = (
        userid => $row->[$h2c{ID}],
        password => md5_base64($row->[$h2c{PIN}]),
        title => $row->[$h2c{TITLE}],
        dateenrolled => fix_date($row->[$h2c{DATE_PRIVILEGE_GRANTED}]),
        dateexpiry => fix_date($row->[$h2c{DATE_PRIVILEGE_EXPIRES}]),
        borrowernotes => scalar(get_ext($ext_data, $row, $h2c{EXTENDED_INFO_OFFSET}, 'NOTES')),
        branchcode => 'NIWA',
        categorycode => 'PT',
        address => '',
        city => '',
    );
    my %ext_attr = (
        DEPT => $row->[$h2c{DEPARTMENT}],
    );
    ($borr{firstname}, $borr{surname}) = figure_name($row->[$h2c{NAME}]);
    my @emails = get_ext($ext_data, $row, $h2c{ADDRESS_OFFSET_1}, 'EMAIL');
    my @email_fields = qw/ email emailpro B_email /;
    while (@email_fields && @emails) {
        $borr{shift @email_fields} = shift @emails;
    }
    die "Too many emails. @$row.\n" if @emails;
    # Remove anything that's undef or empty
    foreach my $key (keys %borr) {
        delete $borr{$key} if !defined($borr{$key});
    }
    foreach my $key (keys %ext_attr) {
        delete $ext_attr{$key} if !defined($ext_attr{$key}) || $ext_attr{$key} =~ m/^\s*$/;
    }
    write_sql($out_fh, \%borr, \%ext_attr);
}
close $fh;
close $out_fh;

sub write_sql {
    my ($fh, $borr, $ext_attr) = @_;

    my $userid = $borr->{userid};
    my @borr_keys = keys %$borr;
    my @borr_values = values %$borr;
    foreach (@borr_values) {
        $_ = sql_escape($_);
    }
    say $fh "INSERT INTO borrowers(".join(",",@borr_keys).") VALUES ('".join("','",@borr_values)."');";
    while (my ($key, $val) = each %$ext_attr) {
        say $fh "INSERT INTO borrower_attributes (borrowernumber, code, attribute) VALUES ((SELECT borrowernumber FROM borrowers WHERE userid='$userid'), '$key', '".sql_escape($val)."');"
    }
}

sub sql_escape {
    my $str = shift;

    $str =~ s/'/\\'/g;
    $str =~ s/"/\\\"/g;
    return $str;
}

# This fixes the date, making it databasy
sub fix_date {
    my ($date) = @_;

    return undef if (!$date);

    # Symphony/Unicorn stores things as DD/MM/YY. 
    my ($dd, $mm, $yy) = $date =~ m#(\d\d)/(\d\d)/(\d\d)#;
    if (!defined($dd) || !defined($mm) || !defined($yy)) {
        warn "Unparsable date: $date\n";
        return undef;
    }
    $yy = ($yy < 50 ? '20' : '19') . $yy;
    return "$yy-$mm-$dd";;
}

# This tries to work out the first and surname from a single field.
sub figure_name {
    my ($name) = @_;

    # Real names are stored as Surname, Firstname. Anything that doesn't have
    # exactly one comma we ignore, and just return it as the surname.
    my ($sn, $fn) = $name =~ m/^([^,]+),\s+([^,]+)$/;
    return (undef, $name) if !defined($sn) || !defined($fn);
    return ($fn, $sn);
}

# This preloads the extended data into a hash so we can quickly look it up
# as needed.
sub load_ext_data {
    my ($fn) = @_;

    my $csv = Text::CSV_XS->new();
    open my $fh, "<:encoding(utf8)", $fn;
    # Read the header and throw it away. It looks like:
    # "OFFSET","ENTRY_POSITION","ABSOLUTE_ENTRY","ENTRY_NUMBER","VED_PREFIX","ENTRY"
    $csv->getline($fh);
    my %res;
    while (my $row = $csv->getline($fh)) {
        $res{$row->[0]}{$row->[2]}{$row->[3]} = $row->[5];
    }
    close $fh;
    return \%res;
}

my $ext_entries;
sub get_ext {
    my ($ext_data, $row, $col_idx, $ext_name) = @_;

    $ext_entries ||=  {
        'NOTES' => '9998',
        'EMAIL' => '9007',
    };

    my $ext_entry = $ext_entries->{$ext_name};
    die "Unknown ext_name: $ext_name" if !$ext_entry;

    my $offset = $row->[$col_idx];
    my @res;
    # Because multiples are seperated by absolute_entry, we enumerate that
    foreach my $abs_entry (sort {$a <=> $b} keys %{$ext_data->{$offset}}) {
        my $val = $ext_data->{$offset}{$abs_entry}{$ext_entry};
        next if !defined($val);
        return $val if !wantarray;
        push @res, $val;
    }
    return undef if !@res;
    return @res;
}

# This outputs the statements required to delete the existing records
sub write_delete {
    my ($fh, $min, $max) = @_;

    if (!defined($min) && !defined($max)) {
        say $fh "TRUNCATE borrowers;";
    } else {
        my @conds;
        push @conds, "borrowernumber < $min" if defined $min;
        push @conds, "borrowernumber > $max" if defined $max;
        my $cond = join(' OR ', @conds);
        $cond = " WHERE " . $cond if $cond;
        say $fh "DELETE FROM borrowers" . $cond . ";";
    }
}
