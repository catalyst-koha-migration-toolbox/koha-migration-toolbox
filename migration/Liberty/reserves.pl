#!/usr/bin/perl

# Copyright 2013 Catalyst IT Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Robin Sheat <robin@catalyst.net.nz> 

# This process a liberty reserves file and loads them in to a Koha database.
# As this format is more broken than most liberty files, the normal CSV
# parser won't work, and we have to do it by hand.

# THIS WILL DELETE ANY EXISTING RESERVES.

# --input: the input file, e.g. waitlist.csv
# --id: the marc field containing the value to match up to liberty IDs, e.g.
#       024_a
# --idprefix: when looking up the liberty ID, attach this prefix to it first
# --branchmap: repeat this for each branch, with libertyname=kohacode
# --debug: a bit more output, maybe.

use strict;
use warnings;

use C4::Context;
use Data::Dumper;
use Getopt::Long;

my ($input_file, $id_marc, $id_prefix, %branch_map, $branch_default);
my ($delete, $debug);

GetOptions(
    'input|i=s'       => \$input_file,
    'id=s'            => \$id_marc,
    'idprefix=s'      => \$id_prefix,
    'branchmap|b=s'   => \%branch_map,
    'branchdefault=s' => \$branch_default,
    'delete'          => \$delete,
    'debug|d+'        => \$debug,
);

die "Need to provide an input file.\n" if !$input_file;

open my $fh, '<', $input_file or die "Unable to open $input_file: $!\n";

my $dbh = C4::Context->dbh;
$dbh->{AutoCommit} = 0;
# skip header
readline $fh;

my $liberty_to_koha_id = make_liberty_to_koha_map();
my $count=0;
if ($delete) {
    $dbh->prepare('DELETE FROM reserves')->execute();
    $dbh->prepare('ALTER TABLE reserves AUTO_INCREMENT=1')->execute();
}
print "Processing reserves\n";
while (<$fh>) {
	if ($count % 70) {
    	print '.';
    } else {
    	print "\n$count ";
    }
    $count++;
	s/[\r\n]//g; # thanks, windows.
	my $row = process_row($_);
	figure_ids($liberty_to_koha_id, $row);
	add_reserves($row) if defined($row->{koha_id});
}
print "\n";
$dbh->commit;

sub process_row {
	my ($line) = @_;
	my ($id, $waitinglist, $dateentered, $datechanged,
	    $timechanged, $whochanged, $notes, $requested) = $line =~
	    /^(\d+),"[^"]*","([^"]*)",(\d+),(\d+),"([\d:]+)","([^"]*)","([^"]*)","[^"]*","(.*)"$/;
	die "'requested' ended up undefined. $id, $waitinglist, $dateentered, $datechanged,
	    $timechanged, $whochanged, $notes\nLine:\n[$line]\n" if !defined($requested);
	# Make this list of requests into something else
    # In some versions of liberty, this info might be in the 'requested'
    # field, apparently.
	my @reqs = split(/,/, $waitinglist);
	my @requested;
	foreach my $r (@reqs) {
	    push @requested, process_request($r);
    }
	my %res = (
	    id          => $id,
	    waitinglist => $waitinglist,
	    dateentered => $dateentered,
	    datechanged => $datechanged,
	    timechanged => $timechanged,
	    whochanged  => $whochanged,
	    notes       => $notes,
	    requested   => \@requested,
    );
    return \%res;
}

# This handles getting the information out of the "waitinglist" field.
# This looks like:
# B00815666 [29/09/2013 15:19]
# or 
# B03819563 [11/09/2013 16:14 A00672908]
# or
# B00836054+ [06/11/2010 10:27 STRATFORD]
# We assume that it's an item barcode if the value isn't in the branchmap.
sub process_request {
    my ($r) = @_;

    my ($user_code, $date, $time, $trailing) = $r =~ /^\s*(.*?)\s\[(.*?) (.*?)(?:\s(.*))?\]\s*$/;
    die "Unable to match $r\n" if !defined $user_code;
    my $pickup = $branch_map{lc($trailing)} if $trailing;
    my $item_number;
    if (!defined $pickup) {
        $pickup = $branch_default;
        # Then it's an item code, possibly.
        if ($trailing) {
            $item_number = get_item_number($trailing);
            if (!defined $item_number) {
                die "Unknown trailing content on $r\n";
            }
        }
    }
    return {
        user_code   => $user_code,
        timestamp   => mysql_date($date),
        pickup      => $pickup,
        item_number => $item_number,
    };
}

# This makes a mapping that allows a Liberty ID be looked up
sub make_liberty_to_koha_map {
    print "Loading ID map\n";
    my ($id_field, $id_subfield) = split(/_/, $id_marc);
    die "$id_marc is not parsable as a marc specification.\n" if (!defined($id_field) || !defined($id_subfield));
    my $query = 'SELECT biblionumber,EXTRACTVALUE(marcxml, \'//record/datafield[@tag="'. $id_field.'"]/subfield[@code="'.$id_subfield.'"]/text()\') FROM biblioitems';
    my $sth = $dbh->prepare($query);
    my $res_code = $sth->execute();
    die "Unable to create liberty to koha ID map.\n" if (!defined($res_code));
    my %map;
    my $count = 0;
    while (my $row = $sth->fetchrow_arrayref) {
    	if ($count % 700) {
    		print '.' if ($count % 10 == 0);
        } else {
        	print "\n$count ";
        }
        $count++;
        $map{$row->[1]} = $row->[0];
    }
    print "\n";
    return \%map;
}

my $item_number_sth;
# Finds the item number given a barcode
sub get_item_number {
    my ($code) = @_;

    if (!$item_number_sth) {
        my $qry = 'SELECT itemnumber FROM items WHERE barcode=?';
        $item_number_sth = $dbh->prepare($qry);
    }
    my $res_code = $item_number_sth->execute($code);
    die "Unable to look up item number.\n" if (!defined $res_code);
    my $row = $item_number_sth->fetchrow_arrayref;
    return undef if !$row;
    return $row->[0];
}

# This uses the provided mapping to convert the liberty IDs in the record into
# something that can be referenced in Koha.
sub figure_ids {
    my ($map, $row) = @_;

    my $id = $row->{id};
    my $koha_id = $map->{$id_prefix . $id};
    warn "Unable to match $id to a Koha ID.\n" if (!defined($koha_id));
    $row->{koha_id} = $koha_id;
}

my $prep_reserves;
sub add_reserves {
    my ($row) = @_;

    my $query = 'INSERT INTO reserves(borrowernumber,reservedate,biblionumber,constrainttype,branchcode,priority,timestamp,lowestPriority,itemnumber) VALUES (?,?,?,\'a\',?,?,now(),0,?)';
    $prep_reserves ||= $dbh->prepare($query);

    my $biblionumber = $row->{koha_id};
    my $count=1;
    foreach my $reserve (@{ $row->{requested} }) {
    	my $user_code = $reserve->{user_code};
    	my $pickup    = $reserve->{pickup};
    	my $reservedate = $reserve->{timestamp};
    	my $itemnumber = $reserve->{itemnumber};
        my $borrowernumber = get_borrowernumber($user_code);
        next if !defined($borrowernumber);
        my $r = $prep_reserves->execute($borrowernumber, $reservedate, $biblionumber,
            $pickup, $count, $itemnumber);
        die "Failed to add record.\nborrowernumber: $borrowernumber, biblionumber: $biblionumber (".$row->{id}."), pickup: $pickup, count: $count" if !defined($r);
        $count++;
    }
}

my $prep_borrnum;
sub get_borrowernumber {
	my ($user_code) = @_;

	my $query = 'SELECT borrowernumber FROM borrowers WHERE cardnumber=?';
	$prep_borrnum ||= $dbh->prepare($query);
	my $res = $prep_borrnum->execute($user_code);
	die "Unable to run borrowernumber query\n" if !defined($res);
	my $row = $prep_borrnum->fetchrow_arrayref;
	if (!$row) {
    	warn "Unable to find card number $user_code\n";
    	return undef;
    }
    return $row->[0];
}

sub mysql_date {
	my ($in) = @_;

	my ($dd, $mm, $yyyy) = $in =~ m{^(..)/(..)/(....)};
	return "$yyyy$mm$dd";
}
