#!/usr/bin/perl
#
# Copyright 2013 Catalyst IT Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Robin Sheat <robin@catalyst.net.nz>

# This parses the CSV-like file that comes out of Liberty and loads it as well
# as possible into Koha.

=head1 NAME

borrowers.pl - convert Liberty borrowers (clients) export and imports it
into Koha

=head1 SYNOPSIS

B<borrowers.pl>
B<--delete>
B<--input>=I<file>
[B<--fieldsep>=I<character>]
[B<--quotechar>=I<character>]
[B<--skipexisting>]
[B<--branchmap>=I<original:code> ...]
[B<--branchdefault>=I<code>]
[B<--categorymap>=I<original:code> ...]
[B<--categorydefault>=I<code>]
[B<--plugin>=I<file>]
[B<--help>] [B<--man>]

=head1 DESCRIPTION

B<borrowers.pl> takes a Liberty borrowers file and imports it into Koha. It
will overwrite all users in the database that have a borrowernumber greater
than 1. As such, it's intended for migration purposes only.

=head1 OPTIONS

=over

=item B<--delete>

This will delete all users first.

=item B<--keep-first>

When used with B<--delete>, don't delete the first borrower entry.

=item B<--input>=I<file>

The CSV file that you want to load.

=item B<--fieldsep>=I<character>

The field separator used by the CSV file.

=item B<--quotechar>=I<character>

The quote character used by the CSV file.

=item B<--skipexisting>

If set, any existing cardnumbers will be skipped, rather than causing an error.

=item B<--branchmap>=I<original:code>

This maps a code in the 'branch' column of the source file to a branch in
Koha. This should be repeated for the number of branches you want to convert.

=item B<--branchdefault>=I<code>

This is the default branch code if nothing else matches.

=item B<--categorymap>=I<original:code>
=item B<--categorydefault>=I<code>

Similar to the equivalent branch options, these apply to the borrower
categories.

=item B<--plugin>=I<file>

This is a perl file that can be used to do custom mapping and processing.
It should evaluate to a hash that provides a mapping for fields and attributes.

=item B<--dryrun>

Do everything, but don't commit it at the end.

=cut

use strict;
use warnings;

use C4::Context;
use Carp;
use Digest::MD5 qw/ md5_base64 /;
use Data::Dumper;
use Encoding::FixLatin qw/ fix_latin /;
use File::Slurp;
use Getopt::Long;
use Text::CSV_XS;
use csvutils;

my ($input_file);

# Probably doesn't need to be customisable
my %sex_map = (
	Male        => 'M',
	Female      => 'F',
	DEFAULT     => undef,
);

my ($ok_to_delete, $debug, $keep_first);
my ($fieldsep, $quote);
my ($help, $man);
my (@branchmap_cli, $branch_default, @categorymap_cli, $category_default);
my ($plugin_file, $skip_existing, $dry_run);

GetOptions(
    'delete'            => \$ok_to_delete,
    'keepfirst'         => \$keep_first,
    'input|i=s'         => \$input_file,
    'debug|d+'          => \$debug,
    'fieldsep=s'        => \$fieldsep,
    'quotechar=s'       => \$quote,
    'branchmap=s'       => \@branchmap_cli,
    'branchdefault=s'   => \$branch_default,
    'categorymap=s'     => \@categorymap_cli,
    'categorydefault=s' => \$category_default,
    'plugin|p=s'        => \$plugin_file,
    'skipexisting'      => \$skip_existing,
    'dryrun'            => \$dry_run,
    'help|h'            => \$help,
    'man'               => \$man,
);

pod2usage( -exitstatus => 0, -verbose => 2 ) if $man;
pod2usage(1) if $help;

my %category_map = make_map($category_default, @categorymap_cli);
my %branch_map = make_map($branch_default, @branchmap_cli);

my $fieldsep_char = csvutils::make_csv_char($fieldsep, ',');
my $quote_char = csvutils::make_csv_char($quote, '"');

die "You need to supply an input file.\n" if (!$input_file);

my %plugins;
if ($plugin_file) {
    open my $pl_fh, '<', $plugin_file;
    my $code = read_file($pl_fh);
    %plugins = eval($code);
    die "Error reading plugin: $@" if $@;
    close $pl_fh;
}

my $csv = Text::CSV_XS->new({
    	binary  => 1,
    	eol     => $/,
    	allow_loose_quotes  => 1,
    	escape_char => '',
    	sep_char    => $fieldsep_char,
        quote_char  => $quote_char,
    	auto_diag   => 2,
    });

open my $csvfile, '<', $input_file
    or die "Unable to open $input_file: $!\n";
# We're not actually going to use the header for anything
my $header_row = $csv->getline($csvfile);
my $count=0;
my %header_to_column = map { lc($_) => $count++ } @$header_row;

my %field_map = (
    alias => [curry( \&literal, 'cardnumber' ),curry( \&export_savevalue ), curry( \&literal, 'userid' ) ],
    name   => curry( \&comma_name,  'firstname',  'initials', 'surname' ),
    branch => curry( \&remap, \%branch_map, 'branchcode' ),
    address    => curry( \&address, 'address', 'address2', 'city', 'zipcode' ),
    altcontact => curry(
        \&address,            'altcontactaddress1',
        'altcontactaddress2', 'altcontactaddress3',
        'altcontactzipcode'
    ),
    phone => curry( \&literal, 'phone' ),
    fax   => curry( \&literal, 'fax' ),     # Polio
    email => curry( \&literal, 'email' ),
    altphone => curry( \&literal, 'altcontactphone' ),
    category => curry( \&category, \%category_map, undef, 'categorycode' ),
    notes    => curry( \&literal,  'contactnote' ),
    alertmsg => curry( \&literal,  'borrowernotes' ),
    password => curry( \&password, 'password' ),
    datereg    => curry( \&date,    'dateenrolled', undef ),
    birthday   => curry( \&date,    'dateofbirth',  undef ),
    expirydate => curry( \&date,    'dateexpiry',   '2100-01-01' ),
    gender     => curry( \&remap,   \%sex_map,      'sex' ),
    notes      => curry( \&literal, 'contactnote' ),
);

# This is a place to put bits of code when hacking the code in a proper
# way is too much hassle.
my @stupid_hacks = (
);

my $dbh = C4::Context->dbh;
$dbh->{AutoCommit} = 0;
$dbh->{RaiseError} = 0;
# Truncate existing records
if ($ok_to_delete) {
    if ($keep_first) {
        $dbh->prepare('DELETE FROM issues')->execute();;
        $dbh->prepare('DELETE FROM borrowers WHERE borrowernumber<>1')->execute();
        $dbh->prepare('ALTER TABLE borrowers AUTO_INCREMENT=2')->execute();
    } else {
        $dbh->prepare('DELETE FROM issues')->execute();;
        $dbh->prepare('DELETE FROM borrowers')->execute();
        $dbh->prepare('ALTER TABLE borrowers AUTO_INCREMENT=1')->execute();
    }

}
$count=0;
# This is needed to set up gurantor relationships
my %alias_to_id;
while (my $row = $csv->getline($csvfile)) {
	if ($count % 70 == 0) {
    	print "\n".$count." ";
    } else {
    	print '.';
    }
    $count++;

    # Fix up anything that is stupid and custom
    foreach my $func (@stupid_hacks) {
    	$func->($row);
    }
    my (@field_names, @field_values);
    my $alias; # need to use this value for caching the alias to id maps
    # A hack to make 'alias' go first
    foreach my $field ('alias', grep { $_ ne 'alias' } keys %field_map) {
        my ($value, @values);
        my @parts = split /,/, $field;
        PART: foreach my $part (@parts) {
            my @or_parts = split /\|/, $part;
            foreach my $p (@or_parts) {
                my $col = $header_to_column{$p};
                die "CSV field '$p' does not exist\n" if (!defined($col));
                my $src_val = clean_string($row->[$header_to_column{$p}]);
                next if (!$src_val);
                push @values, $src_val;
                next PART;
            }
        }
        $value = join(',', @values);
        $alias = $alias || $value || die "The alias value was undefined, record $count\n";
        my $func = $field_map{$field};
        if (ref($func) ne 'ARRAY') {
        	$func = [ $func ];
        }
        foreach my $f (@$func) {
            my ($db, $vals) = $f->($value);
            next if (!$db || !@$db);
            push @field_names, @$db;
            push @field_values, @$vals;
        }
    }
    die "Blank record at $count\n" if (!@field_names || !@field_values);
    if ($skip_existing && (my $cardnumber = borrower_exists(\@field_names, \@field_values))) {
        warn "Skipping duplicate cardnumber $cardnumber\n";
        next;
    }
    my $id = insert_record(\@field_names, \@field_values);
    $alias_to_id{lc $alias} = $id;
    # This is cos liberty sucks
    my @old_aliases = get_old_aliases($row->[$header_to_column{notes}]);
    foreach my $a (@old_aliases) {
    	$alias_to_id{lc $a} = $id;
    }
    # Now do the plugin stuff
    # TODO implement for fields also
    if ($plugins{attributes}) {
        my $attrs = $plugins{attributes};
        foreach my $attr (keys %$attrs) {
        	my $col = $header_to_column{$attr};
        	die "CSV field '$attr' does not exist\n" if (!defined($col));
            my $src_val = clean_string($row->[$header_to_column{$attr}]);
            next if (!defined($src_val) || $src_val eq '');
            my @val = $attrs->{$attr}->($src_val);
            if (!defined($val[1])) {
                warn "Unknown attribute: $src_val\n";
            } else {
                insert_attribute($id, @val);
            }
        }
    }
}
if (!$dry_run) {
    $dbh->commit;
} else {
    $dbh->rollback;
    print "Changes not committed.\n";
}
print "\n";

# This takes the fields and values array that's going to go to the database
# and checks to see if that cardnumber already exists. If it does, it returns
# the cardnumber, otherwise returns undef.
my $borrower_exists_sth;
sub borrower_exists {
    my ($fields, $values) = @_;

    # Find where cardnumber is in $fields
    my $i;
    for ($i=0; $fields->[$i] ne 'cardnumber'; $i++){}
    my $cardnum = $values->[$i];
    $borrower_exists_sth = $dbh->prepare('SELECT cardnumber FROM borrowers WHERE cardnumber=?') unless $borrower_exists_sth;;
    $borrower_exists_sth->execute($cardnum);
    my $row = $borrower_exists_sth->fetchrow_arrayref;
    return $row ? $cardnum : undef;
}

sub insert_record {
    my ($field_names, $field_values) = @_;

    die if !defined($field_values);
    my $dbg_q = "INSERT INTO borrowers (".join(', ', @$field_names).") VALUES (".join(', ', @$field_values).")";
#die Dumper($field_names, $field_values) if grep { !defined($_) } @$field_values;
    print "$dbg_q\n" if ($debug);
    my $qm = join(", ", map { '?' } @$field_values);
    my $q = "INSERT INTO borrowers (".join(', ', @$field_names).") VALUES ($qm)";
    my $sth = $dbh->prepare($q);
    my $res = $sth->execute(@$field_values);
    my $id = $dbh->last_insert_id(undef,undef,undef,undef);
    if (!defined($res)) {
        # Error
        die "Database error:\n".Dumper($field_names) . Dumper($field_values);
    }
    return $id;
}

sub insert_attribute {
    my ($borrower_id, $code, $val) = @_;

    #die "Undefined code or value" if (!defined($code) || !defined($val));
    # $code and $val might be arrays if they were created with an internal
    # function like 'literal'.
    $code = $code->[0] if (ref($code) eq 'ARRAY');
    $val = $val->[0] if (ref($val) eq 'ARRAY');
    my $q = "INSERT INTO borrower_attributes (borrowernumber, code, attribute) VALUES (?,?,?)";
    my $sth = $dbh->prepare($q);
    my $res = $sth->execute($borrower_id, $code, $val);
    if (!defined($res)) {
        die "Database error inserting attribute:\n".Dumper($borrower_id, $code, $val);
    }
}

sub literal {
    my ($db, $value) = @_;

    return if (!$value);
    return ([$db], [$value]);
}

# This attempts to work out the name. It is pretty simple, and if it
# can't make sense of it, it'll just shove the results into the surname. This
# only handles the first and last names, though could be modified to handle
# initials also.
sub name {
	my ($first_db, $middle_db, $last_db, $value) = @_;

    $value =~ s/^\s*(.*?)\s*$/$1/;
	my ($first, $last) = $value =~ /^([^ ]+?) +([^ ]+?)$/;
	if (!defined($first) || !defined($last)) {
        return ([$last_db], [$value]);
    }
    return ([$first_db, $last_db], [$first, $last]);
}

# This handles names in the form of last, surname middle.
sub comma_name {
    my ($first_db, $middle_db, $last_db, $value) = @_;

    my ($last, $first, $middle) = $value =~ m/^(.*), ([^ ]*)(?: (.*))?$/;
    if (!defined($last) || !defined($first)) {
        return ([$last_db], [$value]);
    }
    return ([$first_db, $last_db], [$first, $last]) unless $middle;
    return ([$first_db, $middle_db, $last_db], [$first, $middle, $last]);
}

sub remap {
    my ($map, $db, $value) = @_;

    my $res = $map->{$value};
    if (!$res) {
        if (exists($map->{DEFAULT})) {
            $res = $map->{DEFAULT};
            return () if (!defined($res));
        } else {
            die "No mapping for '$value' (field: $db)\n" if (defined($res));
        }
    }
#    die "$db, $value" if !$res;
    return ([$db], [$res]);
}

sub password {
    my ($db,$value) = @_;

    return ([$db], ['!']) if (!$value);
    my $digest = md5_base64($value);
    return ([$db], [$digest]);
}

sub date {
    my ($db, $default, $value) = @_;

    if (!$value || $value =~ /^0+$/ || $value eq '00/00/0000') { # blank, undef, or 0
        return () if !$default;
        return ([$db], [$default]);
    }

    my $date_out = fix_date($value);
    return ([$db], [$date_out]);
}

# This attempts to guess what date format the input is in, and convert it if
# needed. It'll detect dd/mm/yyyy and yyyy-mm-dd, and outputs the latter
# (which mysql is happy with.) If it detects a null date, it returns undef.
sub fix_date {
    my ($date) = @_;

    return undef if ($date eq '00000000' || $date eq '0000-00-00');

    if ($date =~ m|(\d\d)/(\d\d)/(\d{4})|) {
        return "$3-$2-$1";
    }

    if ($date =~ /\d{4}-\d\d-\d\d/) {
        return $date;
    }

    if ($date =~ /(\d{4})(\d{2})(\d{2})/) {
        return "$1-$2-$3";
    }

    croak "Unexpected date input: $date";
}

sub category {
    # $hb_code is 'housebound code'. This isn't really implemented any more.
    my ($map, $hb_code, $db, $value) = @_;

    my ($cat, $hb) = split /,/, $value;
    return ([$db], [$hb_code]) if ($hb && $hb eq 'YES');
    return ([$db], [$map->{DEFAULT}]) if !$cat;
    my $res = $map->{lc($cat)};
    die "No category mapping for '$value' ($cat) (field: $db)\n" if (!$res);
    return ([$db], [$res]);
}

sub address {
    my ($db_add, $db_add2, $db_city, $db_zip, $value) = @_;

    # This applies some simple heuristics to split the address into parts. It
    # often will fail.
    my @lines = split /[\n\r]+/, $value;
    return if !@lines;
    return ([$db_add], [$value]) if @lines == 1;
    my $lastline = pop @lines;
    my ($city, $zip) = $lastline =~ /^\s*(.*)\s+(\d+)\s*$/;
    my @res = ([], []);
    if (!defined($city)) {
    	if ($lastline =~ /^\s*\d+\s*$/) {
        	push @{$res[0]}, $db_zip;
        	push @{$res[1]}, $lastline;
        	$lastline = pop @lines;
        }
    	push @{$res[0]}, $db_city;
    	push @{$res[1]}, $lastline;
    } else {
    	push @{$res[0]}, $db_city;
    	push @{$res[1]}, $city;
    	push @{$res[0]}, $db_zip;
    	push @{$res[1]}, $zip;
    }
    if (@lines == 1) {
    	push @{$res[0]}, $db_add;
    	push @{$res[1]}, shift @lines;
    } else {
    	push @{$res[0]}, $db_add2;
    	push @{$res[1]}, pop @lines;
    	push @{$res[0]}, $db_add;
    	push @{$res[1]}, join(', ', @lines);
    }
    return @res;
}

# This expects that you are using the 'address' field as the department
# or similar, and that only address2 and city are going to be populated from
# this value.
sub simple_address {
    my ($db_add2, $db_city, $value) = @_;

    my @lines = split /[\n\r]+/, $value;
    return if !@lines;
    return ([$db_add2], [$value]) if @lines == 1;
    s/^\s*(.*?)\s*$/$1/ foreach (@lines); # trim
    return ([$db_add2, $db_city], [join(', ', @lines[0..($#lines-1)]), $lines[-1]]);
}

# This does some mildly NZ-specific manipulation to normalise phone numbers.
# The first arg is the default area code, which is prepended to the number if
# it doesn't start with '0' or '+'. Also, all non-digits (or '+') will go away.
sub phone {
    my ($area, $db_phone, $value) = @_;

    return if !$value;

    $value =~ s/[^+\d]//g;

    return ([$db_phone], [$value]) if $value =~ /^[0+]/;

    return ([$db_phone], [$area . $value]);
}

# This links up a record with a guarantor.
sub guarantor {
	my ($db, $db_notfound, $alias) = @_;

    return () if !$alias;

    my $id = $alias_to_id{lc $alias};

    if (!defined($id)) {
        warn "Unable to find an ID that matches '$alias'\n";
        return ([$db_notfound], [$alias]);
    }
    warn "** Did find $alias\n";
    return ([$db], [$id]);
}

sub get_old_aliases {
	my ($str) = @_;

	my @res;
	while ($str =~ /Old alias = (\w+)/g) {
    	push @res, $1 if $1;
    }
    return @res;
}

my $export_alias_value;
my %export_alias_seen;
sub export_aliases {
    my ($fh, $value) = @_;

    return if !$fh;

    my @lines = split(/[\r\n]+/, $value);
    foreach my $l (@lines) {
    	next if !($l =~ /^Old alias\s?=\s?(.*)\s*$/);
    	warn "Card $1 has already been seen. Would be mapping from $1 to $export_alias_value. Previous was $1 -> $export_alias_seen{$1}\n" if $export_alias_seen{$1} && $export_alias_seen{$1} ne $export_alias_value;
        print $fh "$1,$export_alias_value\n";
        $export_alias_seen{$1} = $export_alias_value;
    }
}

sub export_savevalue {
	($export_alias_value) = @_;
	return undef;
}

sub curry {
    my $func = shift;
    my $args = \@_;
    sub {
        $func->(@$args, @_)
    };
}

sub clean_string {
    my $value = shift;
    # \a (^G, \x007) is a special character that gets turned into a ',' on
    # write. This lets troublesome commas be escaped.
    $value =~ s/\a/,/g;
    $value =~ s/[\r\n]/\n/g;
    # Sometime some things use funny characters for things. This is what one
    # particular liberty dump uses, if we see different things, it's time to
    # pull them out to a command line option
    $value =~ s/\xAE/\n/g;
    $value =~ s/\x96/-/g;
    # Remove some smart quote madness that gets into CSVs
    $value = fix_latin($value) if ref($value) eq '';
    if (ref($value) eq 'ARRAY') {
        $_ = fix_latin($_) foreach (@$value);
    }
    return $value;
}

# This converts a map provided on the command line to a hash that the
# programme can use.
sub make_map {
    my ($def, @map) = @_;

    my %res;
    $res{DEFAULT} = $def if defined $def;
    foreach my $m (@map) {
        my ($a, $b) = split /:/, $m;
        die "The map $m isn't valid.\n" if (!$a || !$b);
        $res{lc $a} = $b;
    }
    return %res;
}
