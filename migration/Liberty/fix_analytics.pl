#!/usr/bin/perl 
#
# Copyright 2013 Catalyst IT Ltd.
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Written by Robin Sheat <robin@catalyst.net.nz>

=head1 NAME

fix_analytics - walks over a MARC file, and configures it for analytics

=head1 SYNOPSIS

B<fix_analytics>
B<--marc>=I<file>
B<--output>=I<file>
B<--man>
B<--help>

=head1 DESCRIPTION

For a serial to be linked to its articles, a few conditions need to be set.
These are documented at http://wiki.koha-community.org/wiki/Analytics. 

This script goes through a MARC file, finds all the records with 773$w
populated and marks them as component parts. Everything that has a control
number pointed to by another record's 773$w has its leader set to make it a
serial.

=head1 OPTIONS

=over

=item B<--marc>

The input MARC file to process.

=item B<--output>

The file that the output is written to.

=cut

use strict;
use warnings;
use autodie;

use Getopt::Long;
use MARC::Batch;
use Pod::Usage;

use Data::Dumper;

my ($input_file, $output_file);
my ($help, $man);

GetOptions(
    'marc=s'    => \$input_file,
    'output=s'  => \$output_file,
    'help|h'    => \$help,
    'man'       => \$man,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage( -exitstatus => 0, -verbose => 2 ) if $man;

pod2usage("An input file must be supplied.\n") if (!$input_file);
pod2usage("An output file must be supplied.\n") if (!$output_file);

# Pass 1 is read-only and just collects all the control numbers and titles and 
# links that we care about.
my %control_numbers;
my %titles;
my %children_of;

my $batch = MARC::Batch->new('USMARC', $input_file);
print "Pass 1\n";
my $count = 0;
while (my $marc = $batch->next) {
    if ($count % 70 == 0) {
        print "\n$count ";
    }
    print ".";
    $count++;
    # Do we have a 773$w?
    my $rcn = $marc->subfield('773', 'w');
    $control_numbers{$rcn} = 1 if defined $rcn;

    # Link titles and control numbers
    my $cn_field = $marc->field('001');
    die "no cn field" if !$cn_field;
    my $cn = $cn_field->data();
    my $title = $marc->subfield('245', 'a');
    $titles{$cn} = $title;

    if (defined $rcn && defined $cn) {
        # Store the things that link to a particular control number (i.e.
        # the children) so we can later create 774 fields.
        push @{ $children_of{$rcn} }, $cn;
    }
}

# Pass 2 we output the new file
open(my $out_fh, '>:utf8', $output_file);
$batch = MARC::Batch->new('USMARC', $input_file);
print "\n\nPass 2\n";
$count = 0;
while (my $marc = $batch->next) {
    if ($count % 70 == 0) {
        print "\n$count ";
    }
    print ".";
    $count++;
    my $rcn = $marc->subfield('773', 'w');
    my $control_num = $marc->field('001')->data();
    if ($rcn) {
        # It's an article type thing
        my $leader = $marc->leader();
        substr($leader, 7, 1) = 'a';
        $marc->leader($leader);
        my $marc773 = $marc->field('773');
        $marc773->update(ind1 => '0');
        $marc773->update(t => $titles{$rcn}) if $titles{$rcn};
    } elsif ($control_numbers{$control_num}) {
        # It's a serial
        my $leader = $marc->leader();
        substr($leader, 7, 1) = 's';
        $marc->leader($leader);
        foreach my $child (@{$children_of{$control_num}}) {
            my $marc774 = MARC::Field->new('774', '0', ' ', w => $child);
            $marc774->update(t => $titles{$child}) if $titles{$child};
            $marc->insert_fields_ordered($marc774);
        }
    }
    $marc->encoding('UTF-8');
    print $out_fh $marc->as_usmarc();
}
close $out_fh;
print "\n";




