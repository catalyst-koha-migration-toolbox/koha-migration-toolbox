#!/usr/bin/perl 

# Copyright 2013 Catalyst IT Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Robin Sheat <robin@catalyst.net.nz> 

# This takes the contents from the liberty Clntchrg table, and turns it (as
# close as it can) into a table for Koha. Fortunately, the table formats are
# fairly similar.
#
# The cardmap stuff is optional and produced by the borrowers convertor, it
# allows borrowers who have changed their card number to be accounted for.
#
# THIS WILL DELETE ANY EXISTING CHARGES

use strict;
use warnings;

use csvutils;

use C4::Context;
use Data::Dumper;
use Getopt::Long;
use Text::CSV_XS;

my ($input_file, $debug, $card_map, $delete);
my ($fieldsep, $quote);

GetOptions(
    'input|i=s'   => \$input_file,
    'cardmap|c=s' => \$card_map,
    'delete'      => \$delete,
    'fieldsep=s'  => \$fieldsep,
    'quotechar=s' => \$quote,
    'debug|d+'    => \$debug,
);

die "Need to provide an input file.\n" if !$input_file;

open my $csvfile, '<', $input_file or die "Unable to open $input_file: $!\n";

my $fieldsep_char = csvutils::make_csv_char($fieldsep, ',');
my $quote_char = csvutils::make_csv_char($quote, '"');
my $csv = Text::CSV_XS->new({
    	eol         => $/,
    	allow_loose_quotes  => 1,
    	escape_char => '',
    	sep_char    => $fieldsep_char,
        quote_char  => $quote_char,
    	auto_diag   => 2,
    	binary      => 1,
    });

my $header_row = $csv->getline($csvfile);
my $count=0;
my %h2c = map { lc($_) => $count++ } @$header_row;

my $dbh = C4::Context->dbh;
$dbh->{AutoCommit} = 0;
$dbh->{RaiseError} = 1;
if ($delete) {
    $dbh->prepare("DELETE FROM accountlines")->execute;
    $dbh->prepare("ALTER TABLE accountlines AUTO_INCREMENT=1")->execute;
}
$count=0;
while (my $row = $csv->getline($csvfile)) {
    if ($count % 70 == 0) {
    	print "\n$count ";
    } else {
    	print '.';
    }
    $count++;
    my @records = make_record($row);
    next if !@records;
    save_record(@records);
}
print "\n";
fix_outstanding();
print "\n";
$dbh->commit;

sub make_record {
	my ($row) = @_;

	my $borrower_code = $row->[$h2c{alias}];
	my $borrower_number = get_borrowernumber($borrower_code);
	return if !defined($borrower_number);
	my $item_code = $row->[$h2c{barcode}];
	my $item_number = get_itemnumber($item_code);
    my @res;
    my $amount = $row->[$h2c{amount}];
    my $account_type = $amount > 0 ? 'FU' : 'Pay';
    my $account_number = get_next_account_number($borrower_number);
    push @res, {
        borrowernumber      => $borrower_number,
        accountnumber       => $account_number,
        itemnumber          => $item_number,
        date                => $row->[$h2c{date}],
        amount              => $amount,
        description         => $row->[$h2c{details}],
        accounttype         => $account_type,
        amountoutstanding   => undef, # to be redone by the 'fix_outstanding'
        # stuff
    };

    return @res;
}

sub get_account_type {
    my ($chargetype) = @_;

    return ('FU', 1)  if ($chargetype eq '');
	return ('Pay', 1);
}

# Tracks the account number for a borrower, and increments it as needed.
my %account_numbers;
sub get_next_account_number {
    my ($borrower_number) = @_;

    if (exists($account_numbers{$borrower_number})) {
    	return ++$account_numbers{$borrower_number};
    } else {
    	return $account_numbers{$borrower_number} = 1;
    }
}

my $prep_save;
my %borrowers_seen;
sub save_record {
    my @records = @_;

    foreach my $row (@records) {
        my $query = 'INSERT INTO accountlines (borrowernumber, accountno, itemnumber, date, amount, description, accounttype, amountoutstanding, timestamp) VALUES (?, ?, ?, ?, ?, ?, ?, ?, now())';
        $prep_save ||= $dbh->prepare($query);
        my $res = $prep_save->execute(
            $row->{borrowernumber},
            $row->{accountnumber},
            $row->{itemnumber},
            $row->{date},
            $row->{amount},
            $row->{description},
            $row->{accounttype},
            $row->{amountoutstanding},
        );
        die "Error inserting data: ".Dumper($row) if !defined($res);
        $borrowers_seen{ $row->{borrowernumber} } = 1;
    }
}

my $prep_borrnum;
my %borrowernumber_cache;
my %card_remap;
sub get_borrowernumber {
	my ($user_code) = @_;

    return $borrowernumber_cache{$user_code} if (exists($borrowernumber_cache{$user_code}));
    %card_remap = load_card_remap($card_map) if ($card_map && !%card_remap);
    $user_code = $card_remap{$user_code} || $user_code if %card_remap;
	my $query = 'SELECT borrowernumber FROM borrowers WHERE cardnumber=?';
	$prep_borrnum ||= $dbh->prepare($query);
	my $res = $prep_borrnum->execute($user_code);
	die "Unable to run borrowernumber query\n" if !defined($res);
	my $row = $prep_borrnum->fetchrow_arrayref;
	if (!$row) {
    	warn "Unable to find borrower card number $user_code\n";
    	$borrowernumber_cache{$user_code} = undef;
    	return undef;
    }
    return $borrowernumber_cache{$user_code} = $row->[0];
}

sub load_card_remap {
	my ($file) = @_;

	my %res;
	open my $fh, '<', $file or die "Unable to open card map file $file: $!\n";
	while (<$fh>) {
        my ($old, $new) = $_ =~ /^(.*),(.*)$/;
    	$res{$old} = $new;
    }
    close $fh;
}

my $prep_itemnum;
sub get_itemnumber {
	my ($barcode) = @_;

    # Some common defaults
    return undef if $barcode eq '' || $barcode eq '(nf)';
	my $query = 'SELECT itemnumber FROM items WHERE barcode=?';
	$prep_itemnum ||= $dbh->prepare($query);
    my $res = $prep_itemnum->execute($barcode);
    die "Unable to run itemnumber query\n" if !defined($res);
    my $row = $prep_itemnum->fetchrow_arrayref;
    # No warning because this will probably happen a lot
    return undef if (!$row);
	return $row->[0];
}

# After all the records are loaded in we need to correct the outstanding
# balance for everyone. 
sub fix_outstanding {
    my $outs_rows_sth = $dbh->prepare(
        'SELECT accountno,amount FROM accountlines WHERE borrowernumber=? ORDER BY accountno ASC'
    );
    my $update_row_sth = $dbh->prepare(
        "UPDATE accountlines SET amountoutstanding=? WHERE borrowernumber=? AND accountno=?"
    );
    my $count = 0;
    $| = 1;
    foreach my $borr_no (sort keys %borrowers_seen) {
    	if ($count % 70) {
            print ".";
        } else {
        	print "\n$count ";
        }
        $count++;
    	$outs_rows_sth->execute($borr_no);
    	my @prev_records;
        while (my $rec = $outs_rows_sth->fetchrow_arrayref) {
            # Run through each record and work out how it changes the value,
            # changing the ones in @prev_records if necessary to make
            # amountoutstanding keep in sync.
            my $acctno = $rec->[0];
            my $amt = $rec->[1];
            if ($amt > 0) {
                # It doesn't change history, it just adds itself.
                push @prev_records, [$acctno, $amt, $amt];
            }
            # If 0 nothing changes
            if ($amt < 0) {
                # Remove this amount from the historical record, they're
                # paying stuff off.
                my $amt_left = $amt;
                foreach my $acc (@prev_records) {
                	last if $amt_left >= 0; # should never be greater than
                	next if $acc->[2] == 0;
                	if ($amt_left <= -$acc->[2]) {
                        # If we're paying off all of this transaction
                        $amt_left += $acc->[2];
                        $acc->[2] = 0; # $acc->[2] is amtoutstanding
                    } else {
                        # If we're paying off part of this transaction
                        $acc->[2] += $amt_left;
                        $amt_left = 0;
                    }
                }
                push @prev_records, [$acctno, $amt, $amt_left];
            }
        }
        # Now save all the changes for this borrower
    	foreach my $r (@prev_records) {
        	$update_row_sth->execute($r->[2], $borr_no, $r->[0]);
        }
    }
}
