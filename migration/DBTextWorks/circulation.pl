#!/usr/bin/perl 

# Copyright 2014 Catalyst IT Ltd.
#
# This file is not part of Koha, but it does work with it.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Written by Robin Sheat <robin@catalyst.net.nz>

=head1 NAME

circulation.pl - migration DBTextWorks circ and loan information to Koha

=head1 DESCRIPTION

This takes current loan and historical stuff from a DBTextWorks catalogue
CSV file and converts them to Koha. Every record has a key value, and it's
expected that that value is in the MARC somewhere to match it up.

This expects a single row in the catalogue to be a single item, because
that's how the data that I'm looking at works. Deduplication should happen
afterwards.

=head1 OPTIONS

=over

=item B<--dryrun>

Pretend to do things, but don't commit at the tend.

=item B<--infile>=I<filename>

The input CSV file.

=item B<--idcolumn>=I<column name>

The name of the column that contains the ID field.

=item B<--idmarc>=I<marc subfield>

The MARC field and subfield containing the ID value to match up, of the form
123_a.

=item B<--namecolumn>=I<column name>

The name of the column that contains the name of the borrower. Default is
"BORROWER".

=item B<--duedatecolumn>=I<column name>

The name of the column that contains the due date of the issue. Default is
"DUE".

=item B<--branch>=I<branch code>

The code for the branch that the items should show as issued from.

=cut

use autodie;
use Carp qw/ croak /;
use C4::Context;
use csvutils;
use Getopt::Long;
use List::MoreUtils qw/ firstidx /;
use MARC::File::XML;
use Modern::Perl;
use Pod::Usage;
use Text::CSV_XS;
use Time::Piece;

my ( $infile, $idcolumn, $idmarc, $dryrun, $branchcode );
my ( $namecolumn, $duecolumn, $oldissuescolumn ) = qw( BORROWER DUE HISTORYReturn );
my ($help);

GetOptions(
    'dryrun'          => \$dryrun,
    'infile=s'        => \$infile,
    'idcolumn=s'      => \$idcolumn,
    'idmarc=s'        => \$idmarc,
    'namecolumn=s'    => \$namecolumn,
    'duedatecolumn=s' => \$duecolumn,
    'oldissuescolumn=s' => \$oldissuescolumn,
    'branchcode=s' => \$branchcode,
    'help'            => \$help,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage("Missing a required argument.\n")
  unless ( $infile && $idcolumn && $idmarc && $namecolumn && $duecolumn );
my $dbh = C4::Context->dbh;
$dbh->{AutoCommit} = 0;
$dbh->{RaiseError} = 0;

my $csv = Text::CSV_XS->new(
    {
        binary             => 1,
        eol                => $/,
        allow_loose_quotes => 0,
        auto_diag          => 2
    }
);
open my $csvfile, '<', $infile;
my $header = $csv->getline($csvfile);
my $id_idx = firstidx { $_ eq $idcolumn } @$header;
my $name_idx = firstidx { $_ eq $namecolumn} @$header;
my $due_idx = firstidx { $_ eq $duecolumn} @$header;
my $oldissue_idx = firstidx { $_ eq $oldissuescolumn} @$header;

my $count=0;
while (my $row = $csv->getline($csvfile)) {
    if ($count % 70 == 0) {
        print "\n$count\t";
    }

    $count++;

    my $item;
    # first thing, if the name is empty, ignore everything else
    my $name = $row->[$name_idx];
    my $id = $row->[$id_idx];
    if (!$name) {
        print ".";
    } else {
        print ",";

        my $due = $row->[$due_idx];

        $due = parse_date($due);
        $item = get_item($id);
        my $borrower = get_borrower($name);
        if (!$borrower) {
            warn "Unable to find borrower for $name\n";
        } else {
            create_issue($item, $borrower, $due);
        }
    }
    my $oldissue = $row->[$oldissue_idx];
    next unless $oldissue;
    my @oldissues = tidy_oldissues($oldissue);
    if (@oldissues) {
        $item = get_item($id) unless $item;
        foreach my $old (@oldissues) {
            my $returned = parse_date($old->{date});
            my $name = $old->{name};
            my $borrower = get_borrower($name);
            next unless $borrower && $returned;
            create_old_issue($item, $borrower, $returned);
        }
    }

}

print "\n\n";
if (!$dryrun) {
    $dbh->commit;
} else {
    $dbh->rollback;
    print "Changes not committed\n";
}

sub parse_date {
    my ($date_str) = @_;

    my $time = eval { Time::Piece->strptime($date_str, '%d/%m/%Y') };
    if ($@) {
        warn "Unable to parse $date_str: $@\n";
        return undef;
    }
    return $time->strftime('%Y-%m-%d');
}

my %item_index;

sub get_item {
    my ($id) = @_;

    build_item_index() unless %item_index;
    my $itemno = $item_index{$id};
    croak "Unable to find an item for $id\n" unless $itemno;
    return $itemno;
}

sub build_item_index {
    my $dbh = C4::Context->dbh;
    my $sth = $dbh->prepare('SELECT itemnumber,marc FROM biblioitems JOIN items ON items.biblionumber=biblioitems.biblionumber');
    $sth->execute();
    my ($tag, $subfield) = $idmarc =~ /(\d\d\d)_(.)/;
    while (my $r = $sth->fetchrow_arrayref) {
        my $itemnum = $r->[0];
        my $marc = $r->[1];
        my $rec = eval {
            MARC::Record->new_from_usmarc($marc);
        };
        if ($@) {
            warn "Skipping the record associated with item $itemnum: $@\n";
            next;
        }
        my $val = $rec->subfield($tag, $subfield);
        die "No ID value found in marc record.\n".$rec->as_formatted."\n" unless $val;
        $item_index{$val} = $itemnum;
    }
}

my %borrower_index;

sub get_borrower {
    my ($name) = @_;

    build_borrower_index() unless %borrower_index;
    my $borrno = $borrower_index{$name};
    return $borrno;
}

sub build_borrower_index {
    my $dbh = C4::Context->dbh;
    my $sth = $dbh->prepare('SELECT borrowernumber, CONCAT(firstname, \' \', surname) AS name FROM borrowers');
    $sth->execute();
    while (my $r = $sth->fetchrow_arrayref) {
        my $borrnum = $r->[0];
        my $name = $r->[1];
        unless ($name) {
            warn "No name found for borrower number $borrnum\n";
            next;
        }
        $borrower_index{$name} = $borrnum;
        # DBTextWorks seems to not always in agreement with itself about
        # hyphenated names
        $name =~ s/-/ /g;
        $borrower_index{$name} = $borrnum;
    }
}

sub create_issue {
    my ($item, $borrower, $due) = @_;

    my $dbh = C4::Context->dbh;
    my $sth = $dbh->prepare('INSERT INTO issues (borrowernumber, itemnumber, date_due, branchcode) VALUES (?,?,?,?)');
    $sth->execute($borrower, $item, $due, $branchcode);
}

sub tidy_oldissues {
    my ($str) = @_;

    my @parts = split('\|', $str);
    my @res;
    foreach my $p (@parts) {
        my ($date, $name) = $p =~ m#(\d+/\d+/\d+)\s*(.*)$#;
        next unless $name;
        $date =~ s/^\s+|\s+$//g;
        $name =~ s/^\s+|\s+$//g;
        push @res, { name => $name, date => $date };
    }
    return @res;
}

sub create_old_issue {
    my ($item, $borrower, $returned) = @_;

    my $dbh = C4::Context->dbh;
    my $sth = $dbh->prepare('INSERT INTO old_issues (borrowernumber, itemnumber, returndate, branchcode) VALUES (?,?,?,?)');
    $sth->execute($borrower, $item, $returned, $branchcode);
}
