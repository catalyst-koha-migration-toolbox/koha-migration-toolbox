#!/usr/bin/perl

# Given a minimal MARC input file with at least:
#  - title
#  - author
# And hopefully:
#  - publisher
#  - publication date
# Perform a search of a set of MARC databases looking for more complete MARC
# records. All results are presented to the user to allow them to select the
# most suitable option.
#
# If the current MARC record is kept, then it is written out to an exceptions
# file to allow further processing with it.
#
# Written by Andrew Ruthven <andrew@etc.gen.nz> to aid with data import for
# Maungaraki Playcentre, Lower Hutt, New Zealand.
#
# License:
#   Copyright 2013, Andrew Ruthven
#
#   All rights reserved.  This program is free software; you can use it and/or
#   distribute it under the same terms as Perl itself; either the latest stable
#   release of Perl when the script was written, or any subsequent stable
#   release.

use strict;
use warnings;
use autodie;
use ZOOM;
use Getopt::Long;
use MARC::File::XML;
use Term::ReadLine;
use Try::Tiny;

my $term = Term::ReadLine->new('MARC Finder');
my $VERBOSE   = 1;
my $SKIP_REST = 0;
my $infile  = undef;
my $outfile = undef;
my $exceptionfile = undef;

GetOptions(
  'i|in=s'    => \$infile,
  'o|out=s'   => \$outfile,
  'e|exception=s' => \$exceptionfile,
  'v|verbose' => \$VERBOSE,
);

my @servers = (
  {
    name => 'LIBRARY OF CONGRESS',
    host => 'lx2.loc.gov',
    port => 210,
    db   => 'LCDB',
    enc  => 'utf8'
  },
  {
    name => 'NATIONAL LIBRARY OF NEW ZEALAND',
    host => 'nlnzcat.natlib.govt.nz',
    port => 7190,
    db   => 'voyager',
    enc  => 'MARC-8'
  },
  {
    name => 'NATIONAL LIBRARY OF AUSTRALIA',
    host => 'catalogue.nla.gov.au',
    port => 7090,
    db   => 'voyager',
    enc  => 'MARC-8',
  },
);

if (defined $exceptionfile) {
    print "Exception file is defined, if you select the current MARC we'll save it to\n";
    print "this file ($exceptionfile) instead.\n";
}

my $exceptionmarc = MARC::File::XML->out( $exceptionfile ) if defined $exceptionfile;

init();

my $inmarc  = MARC::File::XML->in( $infile )
  || die "Failed to open $infile: $!\n";
my $outmarc = MARC::File::XML->out( $outfile ) if defined $outfile;

my %count;
while ( my $marc = $inmarc->next() ) {
    #print $marc->title() . ' / ' . $marc->author() . "\n";

    my $current_marc = 0;

    if (! $SKIP_REST) {
        my $title = $marc->subfield('245', 'a');
        $title =~ s|\s+/\s*$||;

        my $results;
        try {
            $results = search(
                $title,
                $marc->author(),
                $marc->subfield('260','b'),  # Publisher
                $marc->subfield('260','c'),  # Publication Date
            );
        } catch {
            warn "Something went wrong in the search: $_\n";
            warn "Cleaning up, and aborting processing.\n";

            $current_marc = 1;
            $SKIP_REST = 2;
        };
        ($current_marc, $marc) = pick_marc($marc, $results)
            unless $current_marc;

        if ($SKIP_REST && $current_marc) {
            $SKIP_REST = 2;
        }
    }

    if (defined $outfile) {
        if ($current_marc || $SKIP_REST == 2) {
            $count{skipped}++;
            $exceptionmarc->write($marc);

            if ($SKIP_REST == 2) {
                $count{skipped}++;
            } elsif ($SKIP_REST == 1) {
                $count{current}++;
                $SKIP_REST = 2;
            } else {
                $count{current}++;
            }
        } else {
            $count{updated}++;
            $outmarc->write($marc);
        }
    } else {
        print $marc->as_xml() . "\n";
    }
}
$inmarc->close();
undef $inmarc;

if (defined $outmarc) {
    $outmarc->close();
    undef $outmarc;

    print "Current: " . ($count{current} || 0) . "\n";
    print "Updated: " . ($count{updated} || 0) . "\n";
    print "Skipped: " . ($count{skipped} || 0) . "\n";
}

if (defined $exceptionmarc) {
    $exceptionmarc->close();
    undef $exceptionmarc;
}


sub init {
    for my $server (@servers) {
        $server->{conn} = undef if defined $server->{conn};

        eval {
            $server->{conn} = 
                ZOOM::Connection->new($server->{host}, $server->{port},
                                   databaseName => $server->{db},
                                   preferredRecordSyntax => "usmarc",
                );
        };
        if ($@) {
            die "Failed to connect to $server->{name}: ", $@->code(), ": ", $@->message(), "\n";
        }
    }
}

# Try searching with a bunch of different fields set.
sub search {
    my ($title, $author, $publisher, $date) = @_;


    if ($author eq '') {
        $author = undef;
    } else {
        $author =~ s/(\w+),.*/$1/;
    }

    $publisher = undef if $publisher eq '';
    $date      = undef if $date      eq '';

    $publisher =~ s/\.// if defined $publisher;


    my ($found, $results) = _search($title, $author, $publisher, $date);

    if (! $found && defined $publisher && defined $date) {
        print "  Didn't find anything with publisher and publication date. Dropping date.\n";
        ($found, $results) = _search($title, $author, $publisher, undef);

        if (! $found) {
            print "  Didn't find anything with publisher and publication date. Dropping publisher.\n";
            ($found, $results) = _search($title, $author, undef, $date);
        }

        if (! $found) {
            print "  Didn't find anything with publisher and publication date. Dropping both.\n";
            ($found, $results) = _search($title, $author, undef, undef);
        }
    } elsif (! $found && (defined $publisher || defined $date)) {
        print "  Didn't find anything with " . (defined $date ? 'publication date' : 'publisher') . ". Dropping it.\n";
        ($found, $results) = _search($title, $author, undef, undef);
    }

    return $results;
}


sub _search {
    my ($title, $author, $publisher, $date) = @_;

    my @fields;
    push @fields, '@attr 1=4 "'    . $title     . '"';
    push @fields, '@attr 1=1003 "' . $author    . '"' if defined $author;
    push @fields, '@attr 1=1018 "' . $publisher . '"' if defined $publisher;
    push @fields, '@or '
               .  '@attr 1=31 "'   . $date      . '"'
               . ' @attr 1=31 "c'  . $date      . '"' if defined $date;

    my $search = ($#fields >= 1 ? '@and ' : '') . join(' @and ', @fields);
    $search =~ s/(.+ )\@and (.*)/$1$2/;

    print "Searching for $title / " . ($author || 'undef') . "\n";
    my %results;
    my $found = 0;
    print "  $search\n";

    for my $server (@servers) {
        print "  Searching $server->{name}\n"
            if $VERBOSE;

        my $rs = _search_server($server, $search);

        my $n = $rs->size();
        $found ||= $n;

        print "    $n result" . ($n > 1 ? 's' : '') . " found\n";
        $results{$server->{name}} = $rs;
    }

    return ($found, \%results);
}

sub _search_server {
    my ($server, $search) = @_;

    my $count = 0;
    my $attempts = 5;
    while ($count++ < $attempts) {
        my $rs;

        eval {
            $rs = $server->{conn}->search_pqf($search);
        };
        if ($@) {
            if ($@->code() == 10004) {
                warn "Lost connection to $server->{name}, reconnecting.\n";
                init();
                next;
            } else {
                die "Failed to search to $server->{name}: ", $@->code(), ": ", $@->message(), "\n";
            }
        }

        return $rs;
    }

    die "Too many attempts to query $server->{name}, giving up.\n";
}

sub pick_marc {
    my ($marc, $results) = @_;

    my %opts;
    my %special = ( 
      c => 1,
      s => 1,
      q => 1,
    );
    my %keys;

    my $i = 0;
    for my $server (keys %{$results}) {
        for my $n (0..$results->{$server}->size() - 1) {
            print "$i: $server\n";
            print $results->{$server}->record($n)->render();
            print "\n\n";

            push @{ $opts{$server} }, $i;
            $keys{$i} = { server => $server, n => $n };
            $i++;
        }
    }

    print "Current:\n";
    print $marc->as_formatted() . "\n";

    ASK:

    print "Options:\n";
    print " c: use current record\n";
    print " s: use current and skip rest\n";
    print " q: quit, don't do anything further\n";
    for my $server (sort keys %opts) {
      print " $server: " . join(', ', @{ $opts{$server} }) . "\n";
    }
    
    my $ans = $term->readline('MARC Record to use? ');

    if (! defined $keys{$ans} && ! defined $special{$ans}) {
      print "Sorry that isn't a valid option.\n";
      goto ASK;
    }

    if (defined $keys{$ans}) {
        my $server = $keys{$ans}{server};
        my $n      = $keys{$ans}{n};
        $marc = MARC::Record->new_from_usmarc($results->{$server}->record($n)->raw());
    }

    if ($ans eq 's') {
        $SKIP_REST = 1;
    }
    if ($ans eq 'q') {
        print "Time to quit\n";
        exit;
    }

    return ($ans eq 'c' || $ans eq 's', $marc);
}
